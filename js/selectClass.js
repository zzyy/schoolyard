/**
 * Created by zzy on 2019/1/15.
 */
function selectClass() {
  layer.open({
    type: 2,
    area: ['610px', '520px'],
    fix: false, //不固定
    maxmin: true,
    shadeClose: true,
    shade: 0.4,
    title: '班级树',
    content: '../public/classTree.html',
    btn: ['确定', '重置'],
    btnAlign: 'c',
    yes: function(index, layero){
      var iframeWin = window[layero.find('iframe')[0]['name']];
      var $input = iframeWin.document.getElementById('idInput');
      var content = $input.value;
      var id = $input.getAttribute('data-id');
      $('.classes').val(content).attr('data-id', id);
      layer.close(index); //如果设定了yes回调，需进行手工关闭
    },
    btn2: function(index, layero) {
      var iframeWin = window[layero.find('iframe')[0]['name']];
      var $input = iframeWin.document.getElementById('idInput');
      $input.value = '';
      $input.setAttribute('data-id', '');
      $('.classes').val('').attr('data-id', '');
    }
  });
}