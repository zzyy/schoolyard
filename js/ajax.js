/*
* type            请求的方式  默认为post
* url             发送请求的地址
* param           发送请求的参数
* isShowLoader    是否显示loader动画  默认为false
* dataType        返回JSON数据  默认为JSON格式数据
* callBack        请求的回调函数
* */
var public = {
  // 服务器地址
  HTTP_URL: 'http://192.168.1.33:8081/edu-admin'
};

(function() {
  var token = localStorage.getItem('token');

  function $Ajax(opts) {
    this.type         = opts.type || 'post';
    this.url          = opts.url;
    this.param        = opts.param;
    this.isShowLoader = opts.isShowLoader || false;
    this.dataType     = opts.dataType || 'json';
    this.callback     = opts.callback;
    this.contentType  = opts.contentType || 'application/json;charset=UTF-8';
    this.init();
  }

  $Ajax.prototype = {
    // 初始化
    init: function () {
      this.sendRequest();
    },
    // 渲染loader
    showLoader: function() {
      if (this.isShowLoader) {
        var loader = '<div class="ajaxLoader"><div class="loader">加载中...</div></div>';
        $('body').append(loader);
      }
    },
    // 隐藏loader
    hideLoader: function() {
      if(this.isShowLoader){
        $('.ajaxLoader').remove();
      }
    },
    // 发送请求
    sendRequest: function () {
      var self = this;
      $.ajax({
        type: this.type,
        url: public.HTTP_URL + this.url,
        data: JSON.stringify(this.param),
        dataType: this.dataType,
        beforeSend: this.showLoader(),
        headers: {
          'token': token,
          'Content-Type': this.contentType
        },
        success: function(res) {
          self.hideLoader();
          if (res !== null && res !== '') {
            if (self.callback) {
              if (Object.prototype.toString.call(self.callback) === '[object Function]') {
                // Object.prototype.toString.call方法--精确判断对象的类型
                if (res.code === 401) {
                  localStorage.clear();
                  if(window !== top) {
                    top.location.href = '../../login.html';
                  }
                  return false;
                }
                if (res.code === 500 || res.code === 600) {
                  layer.alert(res.message);
                }
                self.callback(res);
              } else {
                console.log('callBack is not a function');
              }
            }
          }
        }
      });
    }
  };
  window.$Ajax = $Ajax;
})();