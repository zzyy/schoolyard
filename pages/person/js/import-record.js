/**
 * Created by zzy on 2019/1/9.
 */
layui.use(['laydate', 'laypage', 'jquery', 'form'], function () {
  var laydate = layui.laydate,
    laypage = layui.laypage,
    $ = layui.jquery,
    form = layui.form;

  // 获取列表
  var param = {
    page: '1',
    limit: '10',
    file_name: '',
    startTime: '',
    endTime: ''
  };
  var detailParam = {
    importId: '',
    limit: '10',
    page: '1'
  };

  getTableData(param, '#tableTpl', '#table');

  // 执行一个laydate实例
  laydate.render({
    elem: '#datetime' //指定元素
    , type: 'datetime'
    , range: '到'
    , format: 'yyyy-MM-dd HH:mm:ss'
  });

  // 点击查询按钮
  form.on('submit(search)', function (data) {
    var df = data.field;
    param.file_name = df.file_name;
    var dateTime = data.field.datetime;
    if (dateTime) {
      var len = dateTime.length;
      param.startTime = dateTime.substring(0, 19);
      param.endTime = dateTime.substring(22, len);
    } else {
      param.startTime = '';
      param.endTime = '';
    }

    getTableData(param, '#tableTpl', '#table');
  });

  // 点击错误数字查看错误详情列表
  $('body').on('click', '.detail', function () {
    detailParam.importId = $(this).attr('data-id');
    layer.open({
      type: 1,
      shadeClose: true,
      btn: '确定',
      btnAlign: 'c',
      area: ['600px', '500px'], //宽高
      content: $('#detail-wrap')
    });
    getDetails(detailParam, '#detailTpl', '#detail');
  });

  // 获取列表方法
  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/biz/personFile/list',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getTableData(detailParam, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }

  // 查询详情列表
  function getDetails(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/biz/personFile/listForDetail',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var detailData = res.content.list;
          var detailTpl = $(tpl).html();
          var detailHtml = juicer(detailTpl, {'detailList': detailData});
          $(el).html(detailHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pagerDetail',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getDetails(detailParam, '#detailTpl', '#detail');
              }
            }
          });
        }
      }
    });
  }
});