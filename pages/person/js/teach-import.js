/**
 * Created by zzy on 2019/1/22.
 */
layui.use('upload', function () {
  var $ = layui.jquery
    ,upload = layui.upload;

  upload.render({ //允许上传的文件后缀
    elem: '#importFile'
    , headers: {token: localStorage.getItem('token')}
    , url: public.HTTP_URL + '/biz/personFile/uploadFile'
    , accept: 'file' //普通文件
    , exts: 'xlsx|xls'
    , before: function (obj) {
      obj.preview(function(index, file, result) {
        if (file.size === 0) {
          return false;
        }
      });

      this.data = {
        fileName: '',
        fileSize: '',
        uploadType: '1',
        fileType: 'xlsx',
        createId: localStorage.getItem('userId'),
        personType: '6000000000'
      };
    }
    , done: function (res) {
      if (res.code === 200) {
        layer.alert(res.voice);
      } else {
        layer.alert(res.message, {
          icon: 5,
          skin: 'layer-ext-moon'
        });
      }
    }
  });


  upload.render({ //允许上传的文件后缀
    elem: '#importImg'
    , headers: {token: localStorage.getItem('token')}
    , url: public.HTTP_URL + '/biz/personFile/uploadFile'
    , accept: 'file' //普通文件
    , exts: 'zip'
    , before: function (obj) {
      $('#imgTips').show();
      obj.preview(function(index, file, result) {
        if (file.size === 0) {
          return false;
        }
      });

      this.data = {
        fileName: '',
        fileSize: '',
        uploadType: '2',
        fileType: 'zip',
        createId: localStorage.getItem('userId'),
        personType: '6000000000'
      };
    }
    , done: function (res) {
      $('#imgTips').hide();
      if (res.code === 200) {
        layer.alert(res.voice);
      } else {
        layer.alert(res.message, {
          icon: 5,
          skin: 'layer-ext-moon'
        });
      }
    }
  });

  // 下载上传文件模板
  $('#downloadTplFile').on('click', function () {
      window.location.href = public.HTTP_URL + "/biz/personFile/downloadTemplet?type=2";
  })
});