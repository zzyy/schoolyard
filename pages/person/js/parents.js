/**
 * Created by zzy on 2019/1/20.
 */
layui.use(['laydate', 'form'], function () {
  var laydate = layui.laydate;
  var laypage = layui.laypage;
  var form = layui.form;

  // 获取家长列表
  var param = {
    id: '',
    page: '1',
    limit: '10',
    userId: localStorage.getItem('userId'),
    personName: '',
    personSex: '',
    cardNum: '',
    childrenName: '',
    childrenNum: '',
    classCode: '',
    GradeCode: ''
  };

  getTableData(param, '#tableTpl', '#table');

  // 点击查询按钮
  form.on('submit(search)', function(data) {
    var df = data.field;
    param.page = 1;
    param.personName = df.name;
    param.cardNum = df.cardNum;
    param.childrenName = df.childName;
    param.childrenNum = df.childNum;
    param.classCode = $('#classes').attr('data-id');
    getTableData(param, '#tableTpl', '#table');
  });

  //执行一个laydate实例
  laydate.render({
    elem: '#datetime' //指定元素
    ,type: 'datetime'
    ,range: '到'
    ,format: 'yyyy-MM-dd HH:mm:ss'
  });

  // 获取家长列表方法
  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/listForParent',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getTableData(param, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }

  $('body').on('click', '.delete', function () {
    var id = $(this).attr('data-id');
    member_del(id);
  });
  /*删除*/
  function member_del(id) {
    layer.confirm('确认要删除吗？', function () {
      //发异步删除数据
      new $Ajax({
        type: 'get',
        url: '/biz/person/delete/' + id + '/6100000000',
        isShowLoader: false,
        dataType: 'json',
        callback: function (res) {
          if (res.code === 200) {
            getTableData(param, '#tableTpl', '#table');
            layer.msg(res.message, {icon: 1, time: 1000});
          }
        }
      });
    });
  }

});

// 点击班级输入框弹出班级树选择
$('.classes').on('click', function () {
    console.log(11);
    selectClass();
});