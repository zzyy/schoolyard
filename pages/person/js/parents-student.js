/**
 * Created by zzy on 2019/1/20.
 */
$(function () {
  var id = getQueryString('id');
  var param = {
    personId: id
  };

  getClass(param);

  function getClass(param) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/listByPar',
      isShowLoader: false,
      dataType: 'json',
      param: param,
      callback: function (res) {
        if (res.code === 200) {
          var students = res.content;
          // $('#studentWrap').html();
          for (var i = 0; i < students.length; i++) {
            var li = '<li><p>'+ students[i].personName +'</p><p>'+ students[i].className +'</p><p>'+ students[i].cardNum +'</p></li>';
            $('#studentWrap').append(li);
          }
        }
      }
    });
  }
});