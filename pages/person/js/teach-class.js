/**
 * Created by zzy on 2019/1/20.
 */
$(function () {
  var id = getQueryString('id');
  var param = {
    personId: id
  };

  getClass(param);

  function getClass(param) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/listByTec',
      isShowLoader: false,
      dataType: 'json',
      param: param,
      callback: function (res) {
        if (res.code === 200) {
          var classes = res.content;
          $('#classWrap').html();
          for (var i = 0; i < classes.length; i++) {
            var li = '<li><p>'+ classes[i].groupNo +'</p><p>'+ classes[i].className +'</p></li>';
            $('#classWrap').append(li);
          }
        }
      }
    });
  }
});