/**
 * Created by zzy on 2019/1/20.
 */
layui.use(['laypage', 'form'], function () {
  var laypage = layui.laypage,
    form = layui.form;

  // 获取学生列表
  var param = {
    page: '1',
    limit: '10',
    userId: localStorage.getItem('userId'),
    personName: '',
    cardNum: '',
    classCode: '',
    GradeCode: ''
  };

  getTableData(param, '#tableTpl', '#table');
  // 点击查询按钮
  form.on('submit(search)', function (data) {
    var df = data.field;
    param.page = 1;
    param.personName = df.name;
    param.cardNum = df.cardNum;
    param.classCode = $('#classes').attr('data-id');
    getTableData(param, '#tableTpl', '#table');
  });

  // 获取学生列表方法
  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/listForClasses',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getTableData(param, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }

  $('body').on('click', '.delete', function () {
    var id = $(this).attr('data-id');
    member_del(id);
  });
  /*删除*/
  function member_del(id) {
    layer.confirm('确认要删除吗？', function () {
      //发异步删除数据
      new $Ajax({
        type: 'get',
        url: '/biz/person/delete/' + id + '/6200000000',
        isShowLoader: false,
        dataType: 'json',
        callback: function (res) {
          if (res.code === 200) {
            getTableData(param, '#tableTpl', '#table');
            layer.msg(res.message, {icon: 1, time: 1000});
          }
        }
      });
    });
  }
});

// 点击班级输入框弹出班级树选择
$('.classes').on('click', function () {
  selectClass();
});