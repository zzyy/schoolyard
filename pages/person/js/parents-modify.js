/**
 * Created by zzy on 2019/1/16.
 */
layui.use(['form', 'layer', 'laydate', 'laypage', 'upload'], function () {
  var form = layui.form
    , layer = layui.layer
    , laydate = layui.laydate
    , laypage = layui.laypage
    , upload = layui.upload;
  var regPhoto = '';
  var onRegPhoto = '';
  var id = getQueryString('id');

    var takePhotoParam = {
        fileBase64: '', // imgBase64.split(',')[1]
        cardNum: '', // $('#cardNum').val()
        personType: '6100000000',
        personId: ''
    };

  // 已添加学生
  var selectStudent = [];
  // 已删除的学生，tag为2的
  var delStudent = [];

  // 获取学生列表
  var param = {
    page: '1',
    limit: '10',
    userId: localStorage.getItem('userId'),
    personName: '',
    cardNum: '',
    classCode: '',
    GradeCode: ''
  };

  renderSelect('sexDic', '#sex');
  renderSelect('raceDic', '#race');
  getTableData(param, '#tableTpl', '#table');
  getDetails(id);

  //自定义验证规则
  form.verify({
    phone: [/^[1]([3-9])[0-9]{9}$/, '请检查手机号码格式是否正确']
    , name: [/^[\u4e00-\u9fa5]{2,10}$/, '姓名必须是2-10位的中文']
    , cardNum: [/^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/, '请检查身份证号码格式是否正确']
    , addr: [/^[a-zA-Z\d\u4e00-\u9fa5（）-]{0,50}$/, '地址不能包含除"（）"和-外的特殊字符且长度小于50位']
  });

  // 点击查询按钮
  form.on('submit(search)', function (data) {
    var df = data.field;
    param.page = 1;
    param.personName = df.name;
    param.cardNum = df.cardNum;
    param.classCode = $('#classes').attr('data-id');
    getTableData(param, '#tableTpl', '#table');
  });

  //拖拽上传
  var uploadImg = upload.render({
    elem: '#photo'
    , headers: {token: localStorage.getItem('token')}
    , data: {
      cardNum: '',
      personType: '6100000000',
      personId: ''
    }
    , url: public.HTTP_URL + '/biz/person/upload'
    , accept: 'images'
    , acceptMime: 'image/jpg'
    , size: 51200
    , before: function (obj) {
      obj.preview(function (index, file, result) {
        $('.upload-content').hide();
        $('.upload-result').show();
        $('#resultImg').attr('src', result);
      });
    }
    , done: function (res) {
      if (res.code === 200) {
        onRegPhoto = res.content;
          regPhoto = res.content;
        layer.msg(res.message);
      } else {
        layer.alert(res.message, {
          icon: 5,
          skin: 'layer-ext-moon'
        });

        $('.upload-content').show();
        $('.upload-result').hide();
        $('#resultImg').attr('src', '');
      }
    }
  });

  $('#cardNum').on('change', function () {
    uploadImg.config.data.cardNum = $(this).val();
  });

  // 监听提交
  form.on('submit(add)', function (data) {
    var df = data.field;

    var addParam = {
      entity: {
        id: id,
        regPhotoUrl: regPhoto,
        onRegPhotoUrl: onRegPhoto,
        personType: '6100000000',
        personName: df.name,
        personSex: df.sex,
        personRace: df.race,
        personAddress: df.addr,
        cardNum: df.cardNum,
        createId: localStorage.getItem('userId'),
        updateId: localStorage.getItem('userId'),
        personState: '0',
        isExamine: '1',
        isEnable: '1',
        createTime: '',
        personPhone: df.phone,
        raceCn: $('#race option:selected').text()
      },
      listParent: [],
      roleIds: []
    };

    if (addParam.entity.personSex === '') {
      layer.msg('请选择性别');
      return false;
    }


    // if (!selectStudent.length) {
    //   layer.alert('请添加绑定学生！', {
    //     icon: 5,
    //     skin: 'layer-ext-moon'
    //   });
    //   return false;
    // } else {
      if (selectStudent.length) {
        for (var i = 0; i < selectStudent.length; i++) {
          var stu = {
            id: '',
            childId: '',
            tag: '',
            parentType: ''
          };
          stu.id = selectStudent[i].id;
          stu.childId = selectStudent[i].childId;
          stu.tag = selectStudent[i].tag;
          addParam.listParent.push(stu);
        }
        // console.log(addParam.listParent);
        // addParam.listParent = addParam.listParent.concat(delStudent);
        // console.log(addParam.listParent);
      }
    // }

    addParents(addParam);
    return false;
  });

  // 删除已添加学生
  $('#bindStudent').on('click', '.close', function () {
    var $student = $(this).parent('.bind-item');
    var id = $student.attr('data-id');
    var childId = $student.attr('data-child-id');
    var stu = {
      id: childId,
      childId: id
    };

    if (!stu.id) {
      selectStudent = removeArr(selectStudent, stu, 'id');
      $student.remove();
    } else {
      $student.hide();
      stu.tag = 2;
      stu.parentType = '';
      delStudent.push(stu);
      selectStudent.forEach(function (item) {
        if (item.id === stu.id) {
          item.tag = 2;
        }
      });
    }
  });
  // 添加学生
  $('body').on('click', '.add-student', function () {
    var id = $(this).attr('data-id');
    var cardNum = $(this).attr('data-num');
    var name = $(this).attr('data-name');
    var stuItem = '<div class="bind-item" data-id="' + id + '" data-child-id="">' +
      ' <i class="layui-icon close">&#x1007;</i><p>' + name + '</p><p>' + cardNum + '</p></div>';

    var stu = {
      id: '',
      childId: id,
      tag: 1
    };


    if (isInArray(delStudent, stu.childId)) {
      delStudent = removeArr(delStudent, stu, 'childId');
      $('.bind-item[data-id="' + stu.childId + '"]').show();
      selectStudent.forEach(function (item) {
        if (item.childId === stu.childId) {
          item.tag = 1;
        }
      });
      return false;
    }

    var isIn = isInArray(selectStudent, stu.childId);
    if (isIn) {
      layer.alert('您已添加过该学生，请勿重复添加！');
    } else {
      selectStudent.push(stu);
      $('#bindStudent').append(stuItem);
    }
  });

  /*删除数组中的某一个对象
   _arr:数组
   _obj:需删除的对象
   */
  function removeArr(_arr, _obj, idType) {
    var length = _arr.length;
    for (var i = 0; i < length; i++) {
      if (_arr[i][idType] === _obj[idType]) {
        if (i === 0) {
          _arr.shift(); //删除并返回数组的第一个元素
          return _arr;
        } else if (i === length - 1) {
          _arr.pop();  //删除并返回数组的最后一个元素
          return _arr;
        } else {
          _arr.splice(i, 1); //删除下标为i的元素
          return _arr;
        }
      }
    }
  }

  // 值是否存在于数组中
  function isInArray(arr, val) {
    var idArr = [];
    for (var i = 0; i < arr.length; i++) {
      idArr.push(arr[i].childId);
    }
    var str = ',' + idArr.join(',') + ',';
    if (str.indexOf(',' + val + ',') !== -1) {
      // 该数存在于数组中
      return true;
    } else {
      // 该数不存在于数组中
      return false;
    }
  }

  // 渲染下拉框
  function renderSelect(key, id) {
    var dic = JSON.parse(localStorage.getItem(key));
    dic.forEach(function (item) {
      var opt = '<option value="' + item.dicClassCode + '">' + item.classNameCn + '</option>';
      $(id).append(opt);
    });
  }

  // 获取学生列表方法
  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/listForClasses',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getTableData(param, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }

  // 修改家长方法
  function addParents(param) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/updatePar',
      isShowLoader: false,
      dataType: 'json',
      param: param,
      callback: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          // 获得frame索引
          var index = parent.layer.getFrameIndex(window.name);
          //关闭当前frame
          parent.layer.close(index);
          parent.location.reload();
        }
      }
    });
  }

  // 家长详情
  function getDetails(id) {
    new $Ajax({
      type: 'get',
      url: '/biz/person/infoPar/' + id,
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var entity = res.content.entity;
          var bindStu = res.content.bindStu;

          $('#name').val(entity.personName);
          $('#sex').val(entity.personSex);
          $('#race').val(entity.personRace);
          $('#birth').val(entity.personBirthday);
          $('#cardNum').val(entity.cardNum);
          $('#addr').val(entity.personAddress);
          $('#phone').val(entity.personPhone);
          $('#resultImg').prop('src', public.HTTP_URL + entity.onRegPhotoUrl);
          regPhoto = entity.regPhotoUrl;

          uploadImg.config.data.cardNum = entity.cardNum;
            uploadImg.config.data.personId = entity.id;
            takePhotoParam.personId = entity.id;

          if (!entity.onRegPhotoUrl) {
            $('.upload-content').show();
            $('.upload-result').hide();
          }
          form.render();

          // 已绑定学生
          for (var i = 0; i < bindStu.length; i++) {
            var stu = {
              id: '',
              childId: '',
              tag: 1
            };
            var item = '<div class="bind-item" data-id="' + bindStu[i].id + '" data-child-id="' + bindStu[i].cpId + '">' +
              ' <i class="layui-icon close">&#x1007;</i><p>' + bindStu[i].personName + '</p><p>' + bindStu[i].cardNum + '</p></div>';
            $('#bindStudent').append(item);

            // 2019年01月23日，周健新加字段，你所看到的id是childId，childId是id
            stu.id = bindStu[i].cpId;
            stu.childId = bindStu[i].id;
            selectStudent.push(stu);
          }
        }
      }
    });
  }

    window.photoHandle = function (data) {
        var aCanvas = document.getElementById('canvas');
        var ctx = aCanvas.getContext('2d');
        var imgBase64 = '';
        ctx.drawImage(data, 0, 0, 300, 225); // 将获取视频绘制在画布上
        $('.upload-content').hide();
        $('.upload-result').show();
        imgBase64 = aCanvas.toDataURL();
        $('#resultImg').attr('src', imgBase64);

        takePhotoParam.fileBase64 = imgBase64.split(',')[1];
        takePhotoParam.cardNum = $('#cardNum').val();

        $.ajax({
            type: 'post',
            url: public.HTTP_URL + '/biz/person/uploadForBase64',
            data: takePhotoParam,
            dataType: 'json',
            headers: {
                'token': localStorage.getItem('token'),
                'Content-Type': this.contentType
            },
            success: function(res) {
                layer.msg(res.message);
                if (res.code === 200) {
                    regPhoto = res.content;
                }
            }
        });
    }

});
// 点击班级输入框弹出班级树选择
$('.classes').on('click', function () {
  selectClass();
});

// 拍照完成后回调函数
function getPhoto(data) {
    photoHandle(data);
}