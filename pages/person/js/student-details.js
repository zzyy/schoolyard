/**
 * Created by zzy on 2019/1/20.
 */
$(function () {
  var id = getQueryString('id');

  getStuDetails(id);
});

function getStuDetails(id) {
  new $Ajax({
    type: 'get',
    url: '/biz/person/infoStu/' + id,
    isShowLoader: false,
    dataType: 'json',
    callback: function (res) {
      if (res.code === 200) {
        var data = res.content.entity;
        var sexList = JSON.parse(localStorage.getItem('sexDic'));

        sexList.forEach(function (sex) {
          if (sex.dicClassCode === data.personSex) {
            data.personSexCn = sex.classNameCn
          }
        });
        $('#photo').attr('src', public.HTTP_URL + data.onRegPhotoUrl);
        $('#name').html(data.personName);
        $('#sex').html(data.personSexCn);
        $('#race').html(data.raceCn);
        $('#birth').html(data.personBirthday);
        $('#cardNum').html(data.cardNum);
        $('#phone').html(data.personPhone);
        $('#addr').html(data.personAddress);
        $('#regTime').html(data.createTime);
        $('#examineTime').html(data.personExamineTime);
        $('#isExamine').html(data.isExamine === '0' ? '审核未通过' : data.isExamine === '1' ? '审核通过' : '待审核'); // 0-审核未通过，1-审核通过，2-待审核
        $('#classes').html(data.className);
      }
    }
  });
}