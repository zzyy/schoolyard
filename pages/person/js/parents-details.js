/**
 * Created by zzy on 2019/1/20.
 */
$(function () {
  var id = getQueryString('id');

  getParDetails(id);
});

function getParDetails(id) {
  new $Ajax({
    type: 'get',
    url: '/biz/person/infoPar/' + id,
    isShowLoader: false,
    dataType: 'json',
    callback: function (res) {
      if (res.code === 200) {
        var entity = res.content.entity;
        var sexList = JSON.parse(localStorage.getItem('sexDic'));

        sexList.forEach(function (sex) {
          if (sex.dicClassCode === entity.personSex) {
            entity.personSexCn = sex.classNameCn
          }
        });
        $('#photo').attr('src', public.HTTP_URL + entity.onRegPhotoUrl);
        $('#name').html(entity.personName);
        $('#sex').html(entity.personSexCn);
        $('#race').html(entity.raceCn);
        $('#birth').html(entity.personBirthday);
        $('#cardNum').html(entity.cardNum);
        $('#phone').html(entity.personPhone);
        $('#addr').html(entity.personAddress);
        $('#regTime').html(entity.createTime);
        $('#examineTime').html(entity.personExamineTime);
        $('#isExamine').html(entity.isExamine === '0' ? '审核未通过' : entity.isExamine === '1' ? '审核通过' : '待审核'); // 0-审核未通过，1-审核通过，2-待审核
        $('#classes').html(entity.className);

        var bindStu = res.content.bindStu;
        for (var i = 0; i < bindStu.length; i++) {
          var sexCn = '';
          sexList.forEach(function (sex) {
            if (sex.dicClassCode === bindStu[i].personSex) {
              sexCn = sex.classNameCn
            }
          });
          var stuItem = '<div class="list-item layui-clear">' +
            '<div class="img-wrap">' +
            '<img src="'+ public.HTTP_URL + bindStu[i].photo +'" alt="">' +
            '</div>' +
            '<ul>' +
            '<li><span>学生姓名：</span><span>'+ bindStu[i].personName +'</span></li>' +
            '<li><span>学生性别：</span><span>'+ sexCn +'</span></li>' +
            '<li><span>学生班级：</span><span>'+ bindStu[i].className +'</span></li>' +
            '<li><span>身份证号：</span><span>'+ bindStu[i].cardNum +'</span></li>' +
            '</ul>' +
            '</div>';
          $('#studentList').append(stuItem);
        }
      }
    }
  });
}