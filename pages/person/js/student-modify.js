/**
 * Created by zzy on 2019/1/16.
 */
layui.use(['form', 'layer', 'laydate', 'upload'], function () {
  var form = layui.form
    , layer = layui.layer
    , laydate = layui.laydate
    , upload = layui.upload;
  var publicParam = { page: 1, limit: 1000 };
  var regPhoto = '';
  var onRegPhoto = '';
  var sum = 0;
  var id = getQueryString('id');

    var takePhotoParam = {
        fileBase64: '', // imgBase64.split(',')[1]
        cardNum: '', // $('#cardNum').val()
        personType: '6200000000',
        personId: ''
    };

  var addParam = {
    entity: {
      id: id,
      regPhotoUrl: '',
      onRegPhotoUrl: '',
      personType: '6200000000',
      personName: '',
      personSex: '',
      personRace: '',
      personAddress: '',
      cardNum: '',
      createId: localStorage.getItem('userId'),
      updateId: localStorage.getItem('userId'),
      personState: '0',
      isExamine: '1',
      isEnable: '1',
      createTime: '',
      personPhone: '',
      raceCn: '',
      sumParent: 0,
      minParent: 0,
      count: 0,
      tag: '0'
    },
    roleIds: [],
    groupIds: []
  };

  renderSelect('sexDic', '#sex');
  renderSelect('raceDic', '#race');

  getClassGroup();

  //自定义验证规则
  form.verify({
    name: [/^[\u4e00-\u9fa5]{2,10}$/, '姓名必须是2-10位的中文']
    , cardNum: [/^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/, '请检查身份证号码格式是否正确']
    , addr: [/^[a-zA-Z\d\u4e00-\u9fa5（）-]{0,50}$/, '地址不能包含除"（）"和-外的特殊字符且长度小于50位']
  });

  //拖拽上传
  var uploadImg = upload.render({
    elem: '#photo'
    , headers: {token: localStorage.getItem('token')}
    , data: {
      cardNum: '',
      personType: '6200000000',
      personId: ''
    }
    , url: public.HTTP_URL + '/biz/person/upload'
    , accept: 'images'
    , acceptMime: 'image/jpg'
    , size: 51200
    , before: function (obj) {
      obj.preview(function(index, file, result) {
        $('.upload-content').hide();
        $('.upload-result').show();
        $('#resultImg').attr('src', result);
      });
    }
    , done: function (res) {
      if (res.code === 200) {
        regPhoto = res.content;
        layer.msg(res.message);
      } else {
        layer.alert(res.message, {
          icon: 5,
          skin: 'layer-ext-moon'
        });

        $('.upload-content').show();
        $('.upload-result').hide();
        $('#resultImg').attr('src', '');
      }
    }
  });

  $('#cardNum').on('change', function () {
    uploadImg.config.data.cardNum = $(this).val();
  });

  // 监听提交
  form.on('submit(add)', function (data) {
    var df = data.field;

    addParam.entity.regPhotoUrl = regPhoto;
    addParam.entity.onRegPhotoUrl = onRegPhoto;
    addParam.entity.personName = df.name;
    addParam.entity.personSex = df.sex;
    addParam.entity.personRace = df.race;
    addParam.entity.personAddress = df.addr;
    addParam.entity.cardNum = df.cardNum;
    addParam.entity.personPhone = df.phone;
    addParam.entity.raceCn = $('#race option:selected').text();
    addParam.groupIds = [];
    addParam.groupIds.push(df.class);

    var count = df.sumParent - addParam.entity.sumParent;
    if (count < 0) {
      addParam.entity.tag = '2'
    } else if (count > 0) {
      addParam.entity.tag = '1'
    } else {
      addParam.entity.tag = '0'
    }
    addParam.entity.count = Math.abs(count);

    if (addParam.entity.personSex === '') {
      layer.msg('请选择性别');
      return false;
    }

    if (addParam.entity.personPhone !== '' && !isPhoneAvailable(addParam.entity.personPhone)) {
      layer.msg('请检查手机号码格式是否正确');
      return false;
    }

    if (typeof df.class === 'undefined') {
      layer.msg('请选择所在班级');
      return false;
    }

    modifyStudent(addParam);
    return false;
  });
  // 加减操作
  $('.calc-btn').on('click', function() {
    var flag = $(this).attr('data-id');
    if (flag === '1') {
      // flag 为真 加1
      sum++;
    } else {
      // flag 为假 减1
      sum--;
      if (sum < addParam.entity.minParent) {
        sum = addParam.entity.minParent;
        layer.msg('不能少于已绑定家长数！');
      }
      if (sum < 0) sum = 0;
    }

    $('#sumParent').val(sum);
  });

  // 获取班级分组
  function getClassGroup() {
    new $Ajax({
      type: 'post',
      url: '/sys/datagroup/list',
      isShowLoader: false,
      dataType: 'json',
      param: publicParam,
      callback: function (res) {
        if (res.code === 200) {
          var classList = res.content.list;
          $('#classCheckBox').html();
          for (var i = 0; i < classList.length; i++) {
            var name = classList[i].gradeName + classList[i].className;
            var classCheckBox = '<div class="layui-col-xs4">' +
              '<input type="radio" name="class" id="class-' + classList[i].id + '" value="' + classList[i].id + '" title="' + name + '" lay-skin="primary">' +
              '</div>';
            $('#classCheckBox').append(classCheckBox);
          }
          // 动态生成表单元素后要重新渲染一下
          form.render();
          getDetails(id);
        }
      }
    });
  }

  // 渲染下拉框
  function renderSelect(key, id) {
    var dic = JSON.parse(localStorage.getItem(key));
    dic.forEach(function (item) {
      var opt = '<option value="' + item.dicClassCode + '">' + item.classNameCn + '</option>';
      $(id).append(opt);
    });
  }

  // 修改学生方法
  function modifyStudent(param) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/updateStu',
      isShowLoader: false,
      dataType: 'json',
      param: param,
      callback: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          // 获得frame索引
          var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
            parent.location.reload();
        } else if (res.code === 500) {
            return false;
        }
      }
    });
  }

  // 学生详情
  function getDetails(id) {
    new $Ajax({
      type: 'get',
      url: '/biz/person/infoStu/' + id,
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var entity = res.content.entity;
          var groupIds = res.content.groupIds;
          $('#name').val(entity.personName);
          $('#sex').val(entity.personSex);
          $('#race').val(entity.personRace);
          $('#birth').val(entity.personBirthday);
          $('#cardNum').val(entity.cardNum);
          $('#addr').val(entity.personAddress);
          $('#phone').val(entity.personPhone);
          $('#resultImg').prop('src', public.HTTP_URL + entity.onRegPhotoUrl);
          getImg(entity.onRegPhotoUrl);
          regPhoto = entity.regPhotoUrl;

          addParam.entity.sumParent = entity.sumParent || 0;
          addParam.entity.minParent = entity.minParent || 0;

          sum = entity.sumParent || 0;
          $('#sumParent').val(sum);

          uploadImg.config.data.cardNum = entity.cardNum;
          uploadImg.config.data.personId = entity.id;
            takePhotoParam.personId = entity.id;

          for (var i = 0; i < groupIds.length; i++) {
            if (document.getElementById("class-" + groupIds[i]) !== null) {
              document.getElementById("class-" + groupIds[i]).setAttribute('checked', 'checked');
            }
          }

          if (!entity.onRegPhotoUrl) {
            $('.upload-content').show();
            $('.upload-result').hide();
          }
          form.render();
        }
      }
    });
  }

  function getImg(url) {
    new $Ajax({
      type: 'get',
      url: url,
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
      }
    });
  }

    window.photoHandle = function (data) {
        var aCanvas = document.getElementById('canvas');
        var ctx = aCanvas.getContext('2d');
        var imgBase64 = '';
        ctx.drawImage(data, 0, 0, 300, 225); // 将获取视频绘制在画布上
        $('.upload-content').hide();
        $('.upload-result').show();
        imgBase64 = aCanvas.toDataURL();
        $('#resultImg').attr('src', imgBase64);

        takePhotoParam.fileBase64 = imgBase64.split(',')[1];
        takePhotoParam.cardNum = $('#cardNum').val();

        $.ajax({
            type: 'post',
            url: public.HTTP_URL + '/biz/person/uploadForBase64',
            data: takePhotoParam,
            dataType: 'json',
            headers: {
                'token': localStorage.getItem('token'),
                'Content-Type': this.contentType
            },
            success: function(res) {
                layer.msg(res.message);
                if (res.code === 200) {
                    regPhoto = res.content;
                }
            }
        });
    }
});

// 拍照完成后回调函数
function getPhoto(data) {
    photoHandle(data);
}