/**
 * Created by zzy on 2019/1/22.
 */
layui.use(['form', 'layer', 'laydate', 'upload', 'laypage'], function () {
  var $ = layui.jquery;
  var form = layui.form
    , layer = layui.layer;
  var laydate = layui.laydate;
  var upload = layui.upload;
  var laypage = layui.laypage;
  var regPhoto = '';

  var takePhotoParam = {
    fileBase64: '', // imgBase64.split(',')[1]
    cardNum: '', // $('#cardNum').val()
    personType: '6100000000',
    personId: ''
  };

  // 已添加学生
  var selectStudent = [];
  // 获取学生列表
  var param = {
    page: '1',
    limit: '10',
    userId: localStorage.getItem('userId'),
    personName: '',
    cardNum: '',
    classCode: '',
    GradeCode: ''
  };

  renderSelect('sexDic', '#sex');
  renderSelect('raceDic', '#race');
  getTableData(param, '#tableTpl', '#table');

  //自定义验证规则
  form.verify({
    phone: [/^[1]([3-9])[0-9]{9}$/, '请检查手机号码格式是否正确']
    ,
    name: [/^[\u4e00-\u9fa5]{2,10}$/, '姓名必须是2-10位的中文']
    ,
    cardNum: [/^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/, '请检查身份证号码格式是否正确']
    ,
    addr: [/^[a-zA-Z\d\u4e00-\u9fa5（）-]{0,50}$/, '地址不能包含除"（）"和-外的特殊字符且长度小于50位']
  });

  // 点击查询按钮
  form.on('submit(search)', function (data) {
    var df = data.field;
    param.page = 1;
    param.personName = df.name;
    param.cardNum = df.cardNum;
    param.classCode = $('#classes').attr('data-id');
    getTableData(param, '#tableTpl', '#table');
  });

  // 提交新增
  form.on('submit(add)', function (data) {
    var df = data.field;

    var addParam = {
      entity: {
        regPhotoUrl: regPhoto,
        personType: '6100000000',
        personName: df.name,
        personSex: df.sex,
        personRace: df.race,
        personAddress: df.addr,
        cardNum: df.cardNum,
        createId: localStorage.getItem('userId'),
        personState: '0',
        isExamine: '1',
        isEnable: '1',
        createTime: '',
        personPhone: df.phone,
        tag: '2',
        raceCn: $('#race option:selected').text()
      },
      listParent: [],
      roleIds: []
    };

    if (addParam.entity.personSex === '') {
      layer.msg('请选择性别');
      return false;
    }

    if (!selectStudent.length) {
      layer.alert('请添加绑定学生！', {
        icon: 5,
        skin: 'layer-ext-moon'
      });
      return false;
    } else {
      if (selectStudent.length) {
        for (var i = 0; i < selectStudent.length; i++) {
          var stu = {
            id: '',
            childId: '',
            parentType: ''
          };
          stu.childId = selectStudent[i];
          addParam.listParent.push(stu);
        }
      }
    }

    addParents(addParam);
    return false;
  });

  //拖拽上传
  var uploadImg = upload.render({
    elem: '#photo'
    , headers: {token: localStorage.getItem('token')}
    , data: {
      cardNum: '',
      personType: '6100000000',
      personId: ''
    }
    , url: public.HTTP_URL + '/biz/person/upload'
    , accept: 'images'
    , acceptMime: 'image/jpg'
    , size: 51200
    , before: function (obj) {
      obj.preview(function (index, file, result) {
        $('.upload-content').hide();
        $('.upload-result').show();
        $('#resultImg').attr('src', result);
      });
    }
    , done: function (res) {
      if (res.code === 200) {
        regPhoto = res.content;
        layer.msg(res.message);
      } else {
        layer.alert(res.message, {
          icon: 5,
          skin: 'layer-ext-moon'
        });

        $('.upload-content').show();
        $('.upload-result').hide();
        $('#resultImg').attr('src', '');
      }
    }
  });

  $('#cardNum').on('change', function () {
    uploadImg.config.data.cardNum = $(this).val();
  });

  // 删除已添加学生
  $('#bindStudent').on('click', '.close', function () {
    var $student = $(this).parent('.bind-item');
    var id = $student.attr('data-id');
    selectStudent.splice($.inArray(id, selectStudent), 1);
    $student.remove();
  });
  // 添加学生
  $('body').on('click', '.add-student', function () {
    var id = $(this).attr('data-id');
    var cardNum = $(this).attr('data-num');
    var name = $(this).attr('data-name');
    var item = '<div class="bind-item" data-id="' + id + '"</div>' +
      ' <i class="layui-icon close">&#x1007;</i><p>' + name + '</p><p>' + cardNum + '</p></div>';

    var isIn = isInArray(selectStudent, id);
    if (isIn) {
      layer.alert('您已添加过该学生，请勿重复添加！');
    } else {
      selectStudent.push(id);
      $('#bindStudent').append(item);
    }
  });

  // 值是否存在于数组中
  function isInArray(arr, val) {
    var str = ',' + arr.join(',') + ',';
    if (str.indexOf(',' + val + ',') !== -1) {
      // 该数存在于数组中
      return true;
    } else {
      // 该数不存在于数组中
      return false;
    }
  }

  // 获取学生列表方法
  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/listForClasses',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getTableData(param, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }

  // 渲染下拉框
  function renderSelect(key, id) {
    var dic = JSON.parse(localStorage.getItem(key));
    dic.forEach(function (item) {
      var opt = '<option value="' + item.dicClassCode + '">' + item.classNameCn + '</option>';
      $(id).append(opt);
    });
    form.render('select');
  }

  // 新增家长方法
  function addParents(param) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/addPar',
      isShowLoader: false,
      dataType: 'json',
      param: param,
      callback: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          // 获得frame索引
          var index = parent.layer.getFrameIndex(window.name);
          //关闭当前frame
          parent.layer.close(index);
          parent.location.reload();
        }
      }
    });
  }

  window.photoHandle = function (data) {
    var aCanvas = document.getElementById('canvas');
    var ctx = aCanvas.getContext('2d');
    var imgBase64 = '';
    ctx.drawImage(data, 0, 0, 300, 225); // 将获取视频绘制在画布上
    $('.upload-content').hide();
    $('.upload-result').show();
    imgBase64 = aCanvas.toDataURL();
    $('#resultImg').attr('src', imgBase64);

    takePhotoParam.fileBase64 = imgBase64.split(',')[1];
    takePhotoParam.cardNum = $('#cardNum').val();

    $.ajax({
      type: 'post',
      url: public.HTTP_URL + '/biz/person/uploadForBase64',
      data: takePhotoParam,
      dataType: 'json',
      headers: {
        'token': localStorage.getItem('token'),
        'Content-Type': this.contentType
      },
      success: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          regPhoto = res.content;
        }
      }
    });
  }
});
// 点击班级输入框弹出班级树选择
$('.classes') .on('click', function () {
  selectClass();
});

// 拍照完成后回调函数
function getPhoto(data) {
  photoHandle(data);
}