/**
 * Created by zzy on 2019/1/16.
 */
layui.use(['form', 'layer', 'laydate', 'upload'], function () {
  var form = layui.form
    , layer = layui.layer
    , laydate = layui.laydate
    , upload = layui.upload;
  var publicParam = {page: 1, limit: 1000};
  var regPhoto = '';
  var onRegPhoto = '';
  var id = getQueryString('id');
  var takePhotoParam = {
    fileBase64: '', // imgBase64.split(',')[1]
    cardNum: '', // $('#cardNum').val()
    personType: '6000000000',
    personId: ''
  };

  renderSelect('sexDic', '#sex');
  renderSelect('raceDic', '#race');

  getClassGroup();

  //自定义验证规则
  form.verify({
    phone: [/^[1]([3-9])[0-9]{9}$/, '请检查手机号码格式是否正确']
    ,
    name: [/^[\u4e00-\u9fa5]{2,10}$/, '姓名必须是2-10位的中文']
    ,
    cardNum: [/^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/, '请检查身份证号码格式是否正确']
    ,
    addr: [/^[a-zA-Z\d\u4e00-\u9fa5（）-]{0,50}$/, '地址不能包含除"（）"和-外的特殊字符且长度小于50位']
  });

  //拖拽上传
  var uploadImg = upload.render({
    elem: '#photo'
    , headers: {token: localStorage.getItem('token')}
    , data: {
      cardNum: '',
      personType: '6000000000',
      personId: ''
    }
    , url: public.HTTP_URL + '/biz/person/upload'
    , accept: 'images'
    , acceptMime: 'image/jpg'
    , size: 51200
    , before: function (obj) {
      obj.preview(function (index, file, result) {
        $('.upload-content').hide();
        $('.upload-result').show();
        $('#resultImg').attr('src', result);
      });
    }
    , done: function (res) {
      if (res.code === 200) {
        onRegPhoto = res.content;
        regPhoto = res.content;
        layer.msg(res.message);
      } else {
        layer.alert(res.message, {
          icon: 5,
          skin: 'layer-ext-moon'
        });

        $('.upload-content').show();
        $('.upload-result').hide();
        $('#resultImg').attr('src', '');
      }
    }
  });

  $('#cardNum').on('change', function () {
    uploadImg.config.data.cardNum = $(this).val();
  });

  // 监听提交
  form.on('submit(add)', function (data) {
    var df = data.field;

    var addParam = {
      entity: {
        id: id,
        regPhotoUrl: regPhoto,
        onRegPhotoUrl: onRegPhoto,
        personType: '6000000000',
        personName: df.name,
        personSex: df.sex,
        personRace: df.race,
        personAddress: df.addr,
        cardNum: df.cardNum,
        createId: localStorage.getItem('userId'),
        updateId: localStorage.getItem('userId'),
        personState: '0',
        isExamine: '1',
        isEnable: '1',
        createTime: '',
        personPhone: df.phone,
        raceCn: $('#race option:selected').text()
      },
      roleIds: getCheckBox('role'),
      groupIds: getCheckBox('class')
    };

    if (addParam.entity.personSex === '') {
      layer.msg('请选择性别');
      return false;
    }

    addTeacher(addParam);
    return false;
  });

  // 获取班级分组
  function getClassGroup() {
    new $Ajax({
      type: 'post',
      url: '/sys/datagroup/list',
      isShowLoader: false,
      dataType: 'json',
      param: publicParam,
      callback: function (res) {
        if (res.code === 200) {
          var classList = res.content.list;
          $('#classCheckBox').html();
          for (var i = 0; i < classList.length; i++) {
            var name = classList[i].gradeName + classList[i].className;
            var classCheckBox = '<div class="layui-col-xs4">' +
              '<input type="checkbox" name="class" id="class-' + classList[i].id + '" value="' + classList[i].id + '" title="' + name + '" lay-skin="primary">' +
              '</div>';
            $('#classCheckBox').append(classCheckBox);
          }
          // 动态生成表单元素后要重新渲染一下
          form.render();

          getRoleGroup();
        }
      }
    });
  }

  // 获取角色分组
  function getRoleGroup() {
    new $Ajax({
      type: 'post',
      url: '/sys/role/list',
      isShowLoader: false,
      dataType: 'json',
      param: publicParam,
      callback: function (res) {
        if (res.code === 200) {
          var roleList = res.content.list;
          $('#roleCheckBox').html();
          for (var i = 0; i < roleList.length; i++) {
            var roleCheckBox = '<div class="layui-col-xs4">' +
              '<input type="checkbox" name="role" id="role-' + roleList[i].roleId + '" value="' + roleList[i].roleId + '" title="' + roleList[i].roleName + '" lay-skin="primary">' +
              '</div>';
            $('#roleCheckBox').append(roleCheckBox);
          }
          // 动态生成表单元素后要重新渲染一下
          form.render();
          getDetails(id);
        }
      }
    });
  }

  // 渲染下拉框
  function renderSelect(key, id) {
    var dic = JSON.parse(localStorage.getItem(key));
    dic.forEach(function (item) {
      var opt = '<option value="' + item.dicClassCode + '">' + item.classNameCn + '</option>';
      $(id).append(opt);
    });
  }

  // 修改老师方法
  function addTeacher(param) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/updateTec',
      isShowLoader: false,
      dataType: 'json',
      param: param,
      callback: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          // 获得frame索引
          var index = parent.layer.getFrameIndex(window.name);
          //关闭当前frame
          parent.layer.close(index);
          parent.location.reload();
        }
      }
    });
  }

  // 老师详情
  function getDetails(id) {
    new $Ajax({
      type: 'get',
      url: '/biz/person/infoTec/' + id,
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var entity = res.content.entity;
          var groupIds = res.content.groupIds;
          var roleIds = res.content.roleIds;
          $('#name').val(entity.personName);
          $('#sex').val(entity.personSex);
          $('#race').val(entity.personRace);
          $('#birth').val(entity.personBirthday);
          $('#cardNum').val(entity.cardNum);
          $('#addr').val(entity.personAddress);
          $('#phone').val(entity.personPhone);
          $('#resultImg').prop('src', public.HTTP_URL + entity.onRegPhotoUrl);
          regPhoto = entity.regPhotoUrl;

          uploadImg.config.data.cardNum = entity.cardNum;
          uploadImg.config.data.personId = entity.id;
          takePhotoParam.personId = entity.id;

          for (var i = 0; i < groupIds.length; i++) {
            if (document.getElementById("class-" + groupIds[i]) !== null) {
              document.getElementById("class-" + groupIds[i]).setAttribute('checked', 'checked');
            }
          }
          for (var j = 0; j < roleIds.length; j++) {
            if (document.getElementById("role-" + roleIds[j]) !== null) {
              document.getElementById("role-" + roleIds[j]).setAttribute('checked', 'checked');
            }
          }

          if (!entity.onRegPhotoUrl) {
            $('.upload-content').show();
            $('.upload-result').hide();
          }
          form.render();
        }
      }
    });
  }

  window.photoHandle = function (data) {
    var aCanvas = document.getElementById('canvas');
    var ctx = aCanvas.getContext('2d');
    var imgBase64 = '';
    ctx.drawImage(data, 0, 0, 300, 225); // 将获取视频绘制在画布上
    $('.upload-content').hide();
    $('.upload-result').show();
    imgBase64 = aCanvas.toDataURL();
    $('#resultImg').attr('src', imgBase64);

    takePhotoParam.fileBase64 = imgBase64.split(',')[1];
    takePhotoParam.cardNum = $('#cardNum').val();

    $.ajax({
      type: 'post',
      url: public.HTTP_URL + '/biz/person/uploadForBase64',
      data: takePhotoParam,
      dataType: 'json',
      headers: {
        'token': localStorage.getItem('token'),
        'Content-Type': this.contentType
      },
      success: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          regPhoto = res.content;
        }
      }
    });
  }
});

// 获取多选框已选中的数据
function getCheckBox(name) {
  var result = [];
  $('input[name="' + name + '"]').each(function () {
    if ($(this).is(':checked')) {
      result.push($(this).attr('value'));
    }
  });
  return result;
}

// 拍照完成后回调函数
function getPhoto(data) {
  photoHandle(data);
}