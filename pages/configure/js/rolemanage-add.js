/**
 * Created by zzy on 2019/1/18.
 */
layui.config({
  base: '../../lib/dtree/'
}).extend({
  treeSelect: 'dtree'
});
layui.use(['form', 'layer', 'dtree'], function () {
  var $ = layui.jquery;
  var form = layui.form
    , layer = layui.layer;
  var dtree = layui.dtree;
  var dtreeMenu, dtreeArea;

  var addParam = {
    roleName: '',
    roleMemo: '',
    createId: localStorage.getItem('userId'),
    areaIdList: [],
    menuIdList: []
  };

  getTree();

  //自定义验证规则
  form.verify({
    number: [/^[^ \r\n\s]{1,10}$/, '编号不能包含空格且不能大于10位']
  });


  //监听新增提交
  form.on('submit(add)', function (data) {
    var menuParam = dtreeMenu.getCheckbarNodesParam();
    var areaParam = dtreeArea.getCheckbarNodesParam();

    if (!menuParam.length) {
      layer.msg('请选择功能菜单！');
      return false;
    }
    if (!areaParam.length) {
      layer.msg('请选择控制区域！');
      return false;
    }

    addParam.roleName = data.field.name;
    addParam.roleMemo = data.field.remark;

    menuParam.forEach(function (item) {
      addParam.menuIdList.push(item.nodeId);
    });
    areaParam.forEach(function (item) {
      addParam.areaIdList.push(item.nodeId);
    });

    addRole();
  });

  // 重置表单
  $('#reset').click(function () {
    dtree.reload(dtreeMenu);
    dtree.reload(dtreeArea);
  });

  // 新增
  function addRole() {
    new $Ajax({
      type: 'post',
      url: '/sys/role/add',
      isShowLoader: false,
      dataType: 'json',
      param: addParam,
      callback: function (res) {
        if (res.code === 200) {
          layer.msg(res.message);
          var index = parent.layer.getFrameIndex(window.name);
          if (res.code === 200){
            parent.layer.close(index);
            parent.location.reload();
          } else if (res.code === 500) {
            return false;
          }
        }
      }
    });
  }

  // 获取树
  function getTree() {
    // 菜单
    new $Ajax({
      type: 'post',
      url: '/sys/menu/select',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.content;
          // 渲染树，使用data渲染
          dtreeMenu = dtree.render({
            elem: '#menu',
            data: data,
            checkbar: true,
            dot: false,
            initLevel: 100,
            skin: 'layui',
            record: true,
            response: {
              title: 'name',
              childName: 'list',
              treeId: 'menuId'
            }
          });
        }
      }
    });
    // 区域
    new $Ajax({
      type: 'post',
      url: '/sys/area/select',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.content;
          // 渲染树，使用data渲染
          dtreeArea = dtree.render({
            elem: '#area',
            data: data,
            checkbar: true,
            dot: false,
            initLevel: 100,
            skin: 'layui',
            record: true,
            response: {
              title: 'name',
              childName: 'list'
            }
          });
        }
      }
    });
  }
});