/**
 * Created by zzy on 2019/1/10.
 */
layui.config({
  base: '../../lib/dtree/'
}).extend({
  treeSelect: 'dtree'
});
layui.use(['form', 'layer', 'dtree', 'laydate'], function () {
  var $ = layui.jquery;
  var form = layui.form
    , layer = layui.layer;
  var dtree = layui.dtree;
  var laydate = layui.laydate;
  var dtreeEle;

  var addParam = {
    classCode: '',
    groupNo: '',
    validityTime: '',
    remark: ''
  };

  getClassesTree();

  // 设置编号前缀
  var d = new Date();
  $('.num-prefix').html(d.getFullYear());

  //执行一个laydate实例
  laydate.render({
    elem: '#endTime' //指定元素
    , type: 'datetime'
    , format: 'yyyy-MM-dd HH:mm:ss'
  });

  //自定义验证规则
  form.verify({
    number: [/^[^ \r\n\s]{1,10}$/, '编号不能包含空格且不能大于10位']
  });

  // 点击树节点获取参数
  dtree.on("node('classesTree')", function (obj) {
    // 如果选择年级不能提交并提示
    if ($.isEmptyObject(obj.parentParam)) {
      layer.msg('您选择的是年级，请选择班级！');
      return false;
    } else {
      $('.class-code').val(obj.param.nodeId);
    }
  });
  //监听提交
  form.on('submit(add)', function (data) {
    if (data.field.classCode === '') {
      layer.msg('请选择班级！', {icon: 5, anim: 6});
      return false;
    }
    addParam.classCode = data.field.classCode;
    addParam.groupNo = d.getFullYear() + data.field.number;
    addParam.validityTime = data.field.endTime;
    addParam.remark = data.field.remark;
    addClassGroup();
  });
  // 重置表单
  $('#reset').click(function () {
    dtree.reload(dtreeEle);
  });
  
  // 新增
  function addClassGroup() {
    new $Ajax({
      type: 'post',
      url: '/sys/datagroup/save',
      isShowLoader: false,
      dataType: 'json',
      param: addParam,
      callback: function (res) {
        layer.msg(res.message);
        var index = parent.layer.getFrameIndex(window.name);
        if (res.code === 200){
          parent.layer.close(index);
          parent.location.reload();
        } else if (res.code === 500) {
          return false;
        }
      }
    });
  }

  // 获取班级树
  function getClassesTree() {
    new $Ajax({
      type: 'post',
      url: '/sys/pzglclassconfig/list1',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.content.list;

          // 渲染树，使用data渲染
          dtreeEle = dtree.render({
            elem: '#classesTree',
            data: data,
            checkbar: true,
            dot: false,
            initLevel: 100,
            skin: 'layui',
            record: true,
            response: {
              title: 'name',
              childName: 'list',
              checkArr: '1'
            }
          });
        }
      }
    });
  }
});