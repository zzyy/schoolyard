/**
 * Created by zzy on 2019/1/18.
 */
$(function () {
  var id = getQueryString('id');
  new $Ajax({
    type: 'post',
    url: '/sys/taskjob/info/' + id,
    isShowLoader: false,
    dataType: 'json',
    callback: function (res) {
      if (res.code === 200) {
        var data = res.content;
        $('#name').html(data.taskName);
        $('#creator').html(data.createUser);
        $('#creTime').html(data.createTime);
        $('#updater').html(data.updateUser);
        $('#updTime').html(data.updateTime);
      }
    }
  });
});