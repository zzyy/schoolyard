/**
 * Created by zzy on 2019/1/17.
 */
layui.use(['laydate', 'form'], function () {
  var laypage = layui.laypage,
      form = layui.form;

  var param = {
    page: 1,
    limit: 10,
    roleName: ''
  };
  getTableData(param, '#tableTpl', '#table');

  // 点击查询按钮
  form.on('submit(search)', function(data) {
    param.page = 1;
    param.roleName = data.field.roleName;
    getTableData(param, '#tableTpl', '#table');
  });

  // 删除回调
  $('body').on('click', '.delete', function () {
    var id = $(this).attr('data-id');
    member_del(id);
  });
  /*删除*/
  function member_del(id) {
    var delParam = [];
    delParam.push(id);
    layer.confirm('确认要删除吗？', function () {
      //发异步删除数据
      new $Ajax({
        type: 'post',
        url: '/sys/role/delete',
        isShowLoader: false,
        param: delParam,
        dataType: 'json',
        callback: function (res) {
          if (res.code === 200) {
            getTableData(param, '#tableTpl', '#table');
            layer.msg(res.message, {icon: 1, time: 1000});
          }
        }
      });
    });
  }

  // 查询列表
  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/sys/role/list',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getTableData(param, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }
});