/**
 * Created by zzy on 2019/1/18.
 */
layui.config({
  base: '../../lib/dtree/'
}).extend({
  treeSelect: 'dtree'
});
layui.use(['form','layer','dtree', 'laydate'], function(){
  $ = layui.jquery;
  var form = layui.form
    ,layer = layui.layer;
  var dtree = layui.dtree;
  var laydate = layui.laydate;
  var modifyParam = {
    id: '',
    classCode: '',
    groupNo: '',
    remark: '',
    validityTime: ''
  };
  var prefix = '';

  var id = getQueryString('id');
  getDetails(id);

  //执行一个laydate实例
  laydate.render({
    elem: '#endTime' //指定元素
    ,type: 'datetime'
    ,format: 'yyyy-MM-dd HH:mm:ss'
  });

  //自定义验证规则
  form.verify({
    number: [/^[^ \r\n\s]{1,10}$/, '编号不能包含空格且不能大于10位']
  });

  // 点击树节点获取参数
  dtree.on("node('classesTree')", function (obj) {
    // 如果选择年级不能提交并提示
    if ($.isEmptyObject(obj.parentParam)) {
      layer.msg('您选择的是年级，请选择班级！');
      return false;
    } else {
      $('.class-code').val(obj.param.nodeId);
    }
  });

  //监听提交
  form.on('submit(modify)', function(data){
    var code = $('#class').attr('data-id');
    if (code === '') {
      layer.msg('请选择班级！', {icon: 5, anim: 6});
      return false;
    }

    modifyParam.id = id;
    modifyParam.classCode = code;
    modifyParam.groupNo = prefix + data.field.number;
    modifyParam.validityTime = data.field.endTime;
    modifyParam.remark = data.field.remark;

    modifyClassGroup();
  });

  $('#reset').click(function() {
    dtree.reload(dtreeEle, {
      url: 'data.json'
    })
  });

  // 查询详情
  function getDetails(id) {
    new $Ajax({
      type: 'post',
      url: '/sys/datagroup/info/' + id,
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.dataGroup;
          // 设置编号前缀
          $('.num-prefix').html(data.groupNo.slice(0, 4));
          prefix = data.groupNo.slice(0, 4);
          // 截取掉年份的其他部分
          $('#number').val(data.groupNo.slice(4, data.groupNo.length));
          $('#class').val(data.className).attr('data-id', data.classCode);
          $('#remark').val(data.remark);
          $('#endTime').val(data.validityTime);
          form.render();
        }
      }
    });
  }

  // 修改
  function modifyClassGroup() {
    new $Ajax({
      type: 'post',
      url: '/sys/datagroup/update',
      isShowLoader: false,
      dataType: 'json',
      param: modifyParam,
      callback: function (res) {
        if (res.code === 200) {
          layer.msg(res.message);
          var index = parent.layer.getFrameIndex(window.name);
          if (res.code === 200){
            parent.layer.close(index);
            parent.location.reload();
          } else if (res.code === 500) {
            return false;
          }
        }
      }
    });
  }

  // 点击区域输入框弹出区域树选择
  $('.class').on('click', function () {
    var self = $(this);
    layer.open({
      type: 2,
      area: ['420px', '420px'],
      fix: false, //不固定
      maxmin: true,
      shadeClose: true,
      shade: 0.4,
      title: '区域',
      content: '../public/classTree.html',
      btn: ['确定', '重置'],
      btnAlign: 'c',
      yes: function (index, layero) {
        var iframeWin = window[layero.find('iframe')[0]['name']];
        var $input = iframeWin.document.getElementById('idInput');
        var content = $input.value;
        var id = $input.getAttribute('data-id');
        self.val(content).attr('data-id', id);
        layer.close(index); //如果设定了yes回调，需进行手工关闭
      },
      btn2: function (index, layero) {
        var iframeWin = window[layero.find('iframe')[0]['name']];
        var $input = iframeWin.document.getElementById('idInput');
        $input.value = '';
        $input.setAttribute('data-id', '');
        self.val('').attr('data-id', '');
      }
    });
  });
});