/**
 * Created by zzy on 2019/1/28.
 */
layui.use(['laydate', 'form'], function () {
  var laydate = layui.laydate;
  var laypage = layui.laypage;
  var form = layui.form;
  var param = {
    page: 1,
    limit: 10,
    sidx: '',
    order: '',
    taskName: '',
    createUser: '',
    startTime: '',
    endTime: '',
    status: ''
  };
  var statusParam = {
    status: '', // 1 正常 0暂停 -1 删除
    id: '',
    userName: localStorage.getItem('userId')
  };

  getTableData(param, '#tableTpl', '#table');

  // 执行一个laydate实例
  laydate.render({
    elem: '#datetime' //指定元素
    , type: 'datetime'
    , range: '到'
    , format: 'yyyy-MM-dd HH:mm:ss'
  });

  //监听提交
  form.on('submit(search)', function (data) {
    param.page = 1;
    param.taskName = data.field.name;
    param.createUser = data.field.user;
    param.startTime = data.field.time;
    getTableData(param, '#tableTpl', '#table');
    return false;
  });


  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/sys/taskjob/list',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getTableData(param, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }

  // 更改任务状态
  $('body').on('click', '.status', function () {
    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');
    statusParam.id = id;
    statusParam.status = status;
    new $Ajax({
      type: 'post',
      url: '/sys/taskjob/taskForPSD',
      isShowLoader: false,
      param: statusParam,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          layer.msg(res.message);
          getTableData(param, '#tableTpl', '#table');
        } else {
          layer.alert(res.message);
        }
      }
    });
  });
});