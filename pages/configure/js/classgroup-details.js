/**
 * Created by zzy on 2019/1/18.
 */
$(function () {
  var id = getQueryString('id');
  new $Ajax({
    type: 'post',
    url: '/sys/datagroup/info/' + id,
    isShowLoader: false,
    dataType: 'json',
    callback: function (res) {
      if (res.code === 200) {
        var data = res.dataGroup;
        $('#groupNo').html(data.groupNo);
        $('#gradeName').html(data.gradeName);
        $('#className').html(data.className);
        $('#remark').html(data.remark);
        $('#createTime').html(data.createTime);
        $('#validityTime').html(data.validityTime);
      }
    }
  });
});