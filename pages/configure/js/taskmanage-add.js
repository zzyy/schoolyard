/**
 * Created by zzy on 2019/1/10.
 */
layui.use(['form','layer', 'element', 'laydate'], function(){
  $ = layui.jquery;
  var form = layui.form
    ,layer = layui.layer;
  var element = layui.element;
  var laydate = layui.laydate;
  var tabIndex = 0;
  var param = {
    taskName: '',
    endTime: '',
    cronResult: '',
    createUser: localStorage.getItem('userId'),
    mtime: '',
    htime: '',
    stime: '',
    dtime: '',
    motime: '',
    ytime: '',
    wtime: '',
    roleIds: '',
    remark: ''
  };

  getRoleList();

  laydate.render({
    elem: '#time'
    ,type: 'time'
    // ,format: 'HH:mm'
    ,range: '至'
  });

  element.on('tab(dayTab)', function(data){
    if (data.index === 0) {
      tabIndex = 0;
    } else if (data.index === 1) {
      tabIndex = 1;
    } else if (data.index === 2) {
      tabIndex = 2;
    }
  });

  //监听提交
  form.on('submit(add)', function(data){
    var df = data.field;
    var times = df.time.split(' 至 ');
    var st = times[0].split(':');
    var et = times[1];

    var nameReg = /^[\u4e00-\u9fa5]{2,10}$/;
    if (!nameReg.test(df.name)) {
      layer.msg('任务名称为2-10位的汉字');
      return false;
    }
    if (getCheckBox('role').length === 0) {
      layer.msg('请选择角色');
      return false;
    }

    param.taskName = df.name;
    param.remark = df.remark;
    param.motime = getCheckBox('month').join(',') || '*';
    param.endTime = et || '*';
    param.htime = st[0] || '*';
    param.mtime = st[1] || '*';
    param.stime = st[2] || '*';
    param.roleIds = getCheckBox('role').join(',');
    param.dtime = '?';
    param.wtime = '?';
    param.ytime = '*';

    switch (tabIndex) {
      case 0:
        if (df.startDay === '' && df.endDay !== '') {
          layer.msg('请选择开始日期');
          return false;
        }
        if (df.startDay !== '' && df.endDay === '') {
          layer.msg('请选择截止日期');
          return false;
        }
        if (Number(df.startDay) > Number(df.endDay)) {
          layer.msg('开始日期不能大于截止日期');
          return false;
        }

        param.dtime = df.startDay + '-' + df.endDay;

        if (df.startDay === '' && df.endDay === '') {
          param.dtime = '*';
        }
        break;
      case 1:
        var days = getCheckBox('day').join(',');
        if (days === '') {
          param.dtime = '*';
        } else {
          param.dtime = getCheckBox('day').join(',');
        }
        break;
      case 2:
        var weeks = getCheckBox('week').join(',');
        if (weeks === '') {
          param.wtime = '*';
        } else {
          param.wtime = getCheckBox('week').join(',');
        }
        break;
    }

    // 秒 分 时 月份中的日期 月份 星期中的日期 年份
    param.cronResult = param.stime + ' ' + param.mtime + ' ' + param.htime + ' ' + param.dtime + ' ' + param.motime + ' ' + param.wtime + ' ' + param.ytime;

    addTask(param);
    return false;
  });

  // 查询角色列表
  function getRoleList() {
    new $Ajax({
      type: 'post',
      url: '/sys/role/select',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.content;
          for(var i = 0; i < data.length; i++) {
            var checkbox = '<div class="layui-form-block"><input type="checkbox" name="role" value="'+ data[i].roleId +'" lay-skin="primary" title="'+ data[i].roleName +'"></div>';
            $('#roleList').append(checkbox);
          }
          form.render();
        }
      }
    });
  }

  // 新增任务
  function addTask(param) {
    new $Ajax({
      type: 'post',
      url: '/sys/taskjob/add',
      isShowLoader: false,
      dataType: 'json',
      param: param,
      callback: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          // 获得frame索引
          var index = parent.layer.getFrameIndex(window.name);
          //关闭当前frame
          parent.layer.close(index);
          parent.location.reload();
        }
      }
    });
  }
});

// 获取多选框已选中的数据
function getCheckBox(name) {
  var result = [];
  $('input[name="' + name + '"]').each(function () {
    if ($(this).is(':checked')) {
      result.push($(this).attr('value'));
    }
  });
  return result;
}