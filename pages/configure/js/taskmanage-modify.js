/**
 * Created by zzy on 2019/1/10.
 */
layui.use(['form','layer', 'element', 'laydate'], function(){
  $ = layui.jquery;
  var form = layui.form
    ,layer = layui.layer;
  var element = layui.element;
  var laydate = layui.laydate;
  var tabIndex = 0;
  var param = {
    id: '',
    taskName: '',
    endTime: '',
    cronResult: '',
    updateUser: localStorage.getItem('userId'),
    mtime: '',
    htime: '',
    stime: '',
    dtime: '',
    motime: '',
    ytime: '',
    wtime: '',
    roleIds: '',
    remark: ''
  };

  getRoleList();
  getTaskDetails();

  laydate.render({
    elem: '#time'
    ,type: 'time'
    // ,format: 'HH:mm'
    ,range: '至'
  });

  element.on('tab(dayTab)', function(data){
    if (data.index === 0) {
      tabIndex = 0;
    } else if (data.index === 1) {
      tabIndex = 1;
    } else if (data.index === 2) {
      tabIndex = 2;
    }
  });

  //监听提交
  form.on('submit(add)', function(data){
    var df = data.field;
    var times = df.time.split(' 至 ');
    var st = times[0].split(':');
    var et = times[1];

    var nameReg = /^[\u4e00-\u9fa5]{2,10}$/;
    if (!nameReg.test(df.name)) {
      layer.msg('任务名称为2-10位的汉字');
      return false;
    }
    if (getCheckBox('role').length === 0) {
      layer.msg('请选择角色');
      return false;
    }

    param.id = getQueryString('id');
    param.taskName = df.name;
    param.remark = df.remark;
    param.motime = getCheckBox('month').join(',') || '*';
    param.endTime = et || '*';
    param.htime = st[0] || '*';
    param.mtime = st[1] || '*';
    param.stime = st[2] || '*';
    param.roleIds = getCheckBox('role').join(',');
    param.dtime = '?';
    param.wtime = '?';
    param.ytime = '*';

    switch (tabIndex) {
      case 0:
        if (df.startDay === '' && df.endDay !== '') {
          layer.msg('请选择开始日期');
          return false;
        }
        if (df.startDay !== '' && df.endDay === '') {
          layer.msg('请选择截止日期');
          return false;
        }
        if (Number(df.startDay) > Number(df.endDay)) {
          layer.msg('开始日期不能大于截止日期');
          return false;
        }

        param.dtime = df.startDay + '-' + df.endDay;

        if (df.startDay === '' && df.endDay === '') {
          param.dtime = '*';
        }
        break;
      case 1:
        var days = getCheckBox('day').join(',');
        if (days === '') {
          param.dtime = '*';
        } else {
          param.dtime = getCheckBox('day').join(',');
        }
        break;
      case 2:
        var weeks = getCheckBox('week').join(',');
        if (weeks === '') {
          param.wtime = '*';
        } else {
          param.wtime = getCheckBox('week').join(',');
        }
        break;
    }

    // 秒 分 时 月份中的日期 月份 星期中的日期 年份
    param.cronResult = param.stime + ' ' + param.mtime + ' ' + param.htime + ' ' + param.dtime + ' ' + param.motime + ' ' + param.wtime + ' ' + param.ytime;

    modifyTask(param);
    return false;
  });

  // 查询角色列表
  function getRoleList() {
    new $Ajax({
      type: 'post',
      url: '/sys/role/select',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.content;
          for(var i = 0; i < data.length; i++) {
            var checkbox = '<div class="layui-form-block"><input type="checkbox" name="role" id="role-' + data[i].roleId + '" value="'+ data[i].roleId +'" lay-skin="primary" title="'+ data[i].roleName +'"></div>';
            $('#roleList').append(checkbox);
          }
          form.render();
        }
      }
    });
  }

  // 获取任务详情
  function getTaskDetails() {
    var id = getQueryString('id');
    new $Ajax({
      type: 'post',
      url: '/sys/taskjob/info/' + id,
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.content;
          var roleIds = data.roleIds.split(',');
          var months = data.motime.split(',');
          var startTime = data.htime + ':' + data.mtime + ':' + data.stime;
          $('#name').val(data.taskName);
          $('#remark').val(data.remark);
          $('#time').val(startTime + ' 至 ' + data.endTime);

          for (var i = 0; i < roleIds.length; i++) {
            if (document.getElementById("role-" + roleIds[i]) !== null) {
              document.getElementById("role-" + roleIds[i]).setAttribute('checked', 'checked');
            }
          }

          for (var j = 0; j < months.length; j++) {
            if (document.getElementById("month-" + months[j]) !== null) {
              document.getElementById("month-" + months[j]).setAttribute('checked', 'checked');
            }
          }

          if (data.dtime === '?') {
            element.tabChange('dayTab', '2');
            tabIndex = 2;
            for (var k = 0; k < data.wtime.length; k++) {
              if (document.getElementById("week-" + data.wtime[k]) !== null) {
                document.getElementById("week-" + data.wtime[k]).setAttribute('checked', 'checked');
              }
            }
          } else if (data.dtime.indexOf(',') !== -1) {
            element.tabChange('dayTab', '1');
            tabIndex = 1;
            var dTime = data.dtime.split(',');
            for (var p = 0; p < dTime.length; p++) {
              if (document.getElementById("day-" + dTime[p]) !== null) {
                document.getElementById("day-" + dTime[p]).setAttribute('checked', 'checked');
              }
            }
          } else if (data.dtime.indexOf('-') !== -1) {
            element.tabChange('dayTab', '0');
            tabIndex = 0;
            var days = data.dtime.split('-');
            $('#startDay').val(days[0]);
            $('#endDay').val(days[1]);
          }
          form.render();
        }
      }
    });
  }

  // 修改任务
  function modifyTask(param) {
    new $Ajax({
      type: 'post',
      url: '/sys/taskjob/update',
      isShowLoader: false,
      dataType: 'json',
      param: param,
      callback: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          // 获得frame索引
          var index = parent.layer.getFrameIndex(window.name);
          //关闭当前frame
          parent.layer.close(index);
          parent.location.reload();
        }
      }
    });
  }
});

// 获取多选框已选中的数据
function getCheckBox(name) {
  var result = [];
  $('input[name="' + name + '"]').each(function () {
    if ($(this).is(':checked')) {
      result.push($(this).attr('value'));
    }
  });
  return result;
}