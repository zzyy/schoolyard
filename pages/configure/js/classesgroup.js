/**
 * Created by zzy on 2019/1/9.
 */
layui.use(['laydate', 'form'], function () {
  var laypage = layui.laypage;
  var form = layui.form;
  var param = {
    page: 1,
    limit: 10,
    groupNo: '',
    gradeName: '',
    className: ''
  };
  //监听提交
  form.on('submit(search)', function(data){
    param.page = 1;
    param.groupNo = data.field.groupNo;
    param.gradeName = data.field.gradeName;
    param.className = data.field.className;
    getTableData(param, '#tableTpl', '#table');
    return false;
  });

  // 获取班级分组列表
  getTableData(param, '#tableTpl', '#table');
  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/sys/datagroup/list',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function(obj, first){
              if (!first) {
                data.page = obj.curr;
                getTableData(param, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }

  // 删除回调
  $('body').on('click', '.delete', function () {
    var id = $(this).attr('data-id');
    deleteHandle(id);
  });
  // 删除方法
  function deleteHandle (id) {
    var data = {
      ids: []
    };
    data.ids.push(id);
    layer.confirm('您确定要删除吗？', function() {
      new $Ajax({
        type: 'post',
        url: '/sys/datagroup/delete',
        isShowLoader: false,
        param: data,
        dataType: 'json',
        callback: function (res) {
          if (res.code === 200) {
            getTableData(param, '#tableTpl', '#table');
            layer.msg(res.message, {icon: 1, time: 1000});
          }
        }
      });
    });
  }
});