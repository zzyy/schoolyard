layui.use(['laydate', 'form'], function () {
  var laydate = layui.laydate;
  var laypage = layui.laypage;
  var form = layui.form;
  var param = {
    page: 1,
    limit: 10,
    title: '',
    content: '',
    className: '',
    startTime: '',
    endTime: ''
  };

  // 执行一个laydate实例
  laydate.render({
    elem: '#datetime' //指定元素
    , type: 'datetime'
    , range: '到'
    , format: 'yyyy-MM-dd HH:mm:ss'
  });

  //监听提交
  form.on('submit(sreach)', function (data) {
    param.page = 1;
    param.title = data.field.title;
    param.content = data.field.content;
    param.className = data.field.className;
    var dateTime = data.field.datetime;
    if (dateTime) {
      var len = dateTime.length;
      param.startTime = dateTime.substring(0, 19);
      param.endTime = dateTime.substring(22, len);
    } else {
      param.startTime = '';
      param.endTime = '';
    }

    getTableData(param, '#tableTpl', '#table');
    return false;
  });

  // 查询通知
  getTableData(param, '#tableTpl', '#table');
  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/biz/notice/list',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getTableData(param, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }
});