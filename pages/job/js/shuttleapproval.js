/**
 * Created by zzy on 2019/1/14.
 */
layui.use(['laydate', 'element', 'form'], function () {
  var laydate = layui.laydate,
      laypage = layui.laypage,
      element = layui.element,
      form = layui.form;
  var shuttleApply = {
    page: 1,
    limit: 10,
    validityDate: '',
    childName: '',
    classCode: '',
    shuttleName: '',
    shuttlePhone: '',
    shuttleId: '',
    type: 0
  };

  getTableData(shuttleApply, '#djTableTpl', '#djTable', 'pager1');

  //执行一个laydate实例
  laydate.render({
    elem: '#datetime1' //指定元素
    ,type: 'date'
    ,format: 'yyyy-MM-dd'
  });
  laydate.render({
    elem: '#datetime2' //指定元素
    ,type: 'date'
    ,format: 'yyyy-MM-dd'
  });

  // 待接审批条件查询
  form.on('submit(sreach1)', function (data) {
    shuttleApply.page = 1;
    shuttleApply.type = 0;
    shuttleApply.childName = data.field.student;
    shuttleApply.applyName = data.field.parent;
    shuttleApply.validityDate = data.field.datetime1;
    shuttleApply.shuttleName = data.field.shuttleName;
    shuttleApply.shuttleId = data.field.type;
    shuttleApply.classCode = $('#classes1').attr('data-id');
    getTableData(shuttleApply, '#djTableTpl', '#djTable', 'pager1');
    return false;
  });

  // 待送审批条件查询
  form.on('submit(sreach2)', function (data) {
    shuttleApply.page = 1;
    shuttleApply.type = 1;
    shuttleApply.childName = data.field.student;
    shuttleApply.applyName = data.field.parent;
    shuttleApply.validityDate = data.field.datetime1;
    shuttleApply.shuttleName = data.field.shuttleName;
    shuttleApply.shuttleId = data.field.type;
    shuttleApply.classCode = $('#classes2').attr('data-id');
    getTableData(shuttleApply, '#dsTableTpl', '#dsTable', 'pager2');
    return false;
  });

  //一些事件监听
  element.on('tab(myTab)', function(data){
    if (data.index === 0) {
      shuttleApply.type = 0;
      getTableData(shuttleApply, '#djTableTpl', '#djTable', 'pager1');
    } else if (data.index === 1) {
      shuttleApply.type = 1;
      getTableData(shuttleApply, '#dsTableTpl', '#dsTable', 'pager2');
    }
  });

  // 查询审批列表
  function getTableData(data, tpl, el, pager) {
    new $Ajax({
      type: 'post',
      url: '/biz/shuttleapply/list',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('.count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: pager,
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function(obj, first){
              if (!first) {
                data.page = obj.curr;
                getTableData(data, tpl, el, pager);
              }
            }
          });
        }
      }
    });
  }
});

// 点击班级输入框弹出班级树选择
$('.classes').on('click', function () {
  selectClass();
});