layui.config({
  base: '../../lib/dtree/'
}).extend({
  treeSelect: 'dtree'
});
layui.use(['form', 'layer', 'dtree'], function () {
  var $ = layui.jquery;
  var form = layui.form
    , layer = layui.layer;
  var dtree = layui.dtree;
  var dtreeEle;

  getClassesTree();

  //自定义验证规则
  form.verify({
    title: [/^[^ \r\n\s]{1,10}$/, '标题不能包含空格且不能大于10位']
    , content: [/^[^ \r\n\s]{1,500}$/, '内容不能包含空格且不能大于500位']
  });

  //监听提交
  form.on('submit(add)', function (data) {
    // 原插件是获取checkbox对象，修改为直接获取id
    var selectNode = dtree.getCheckbarNodesParam('classesTree');
    var addParam = {
      title: data.field.title,
      content: data.field.content,
      personType: data.field.role,
      classCode: []
    };
    if (selectNode.length) {
      console.log(selectNode);
      for (var i = 0; i < selectNode.length; i++) {
        addParam.classCode.push(selectNode[i].nodeId);
      }
    } else {
      layer.msg('请选择班级！');
      return false;
    }

    // 新增通知接口
    new $Ajax({
      type: 'post',
      url: '/biz/notice/add',
      isShowLoader: false,
      dataType: 'json',
      param: addParam,
      callback: function (res) {
        if (res.code === 200) {
          layer.msg(res.message);
          // 获得frame索引
          var index = parent.layer.getFrameIndex(window.name);
          //关闭当前frame
          parent.layer.close(index);
          parent.location.reload();
        }
      }
    });
    return false;
  });

  $('#reset').click(function () {
    dtree.reload(dtreeEle, {
      url: 'data.json'
    })
  });

  // 获取班级树
  function getClassesTree() {
    new $Ajax({
      type: 'post',
      url: '/sys/pzglclassconfig/list1',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.content.list;
          var treeArr = [];

          for (var i = 0; i < data.length; i++) {
            var obj = {
              id: data[i].id,
              title: data[i].name,
              parentId: data[i].parentId,
              checkArr: [{type: '0', isChecked: '0'}],
              children: []
            };
            var list = data[i].list;
            if (list) {
              for (var j = 0; j < list.length; j++) {
                var cObj = {
                  id: list[j].id,
                  title: list[j].name,
                  parentId: list[j].parentId,
                  checkArr: [{type: '0', isChecked: '0'}],
                  children: []
                };
                obj.children.push(cObj);
              }
            }
            treeArr.push(obj);
          }

          // 渲染树，使用data渲染
          dtreeEle = dtree.render({
            elem: '#classesTree',
            data: treeArr,
            checkbar: true,
            dot: false,
            skin: 'layui'
          });
        }
      }
    });
  }
});