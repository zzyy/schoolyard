layui.use(['laydate', 'form'], function () {
  var laydate = layui.laydate;
  var laypage = layui.laypage;
  var form = layui.form;
  var param = {
    page: 1,
    limit: 20,
    childName: '',
    status: '',
    type: ''
  };

  //监听提交
  form.on('submit(sreach)', function (data) {
    param.page = 1;
    param.childName = data.field.student;
    param.status = data.field.state;
    param.type = data.field.type;
    param.className = data.field.className;
    var dateTime = data.field.datetime;
    param.startTime = dateTime.substring(0, 19);
    var len = dateTime.length;
    param.endTime = dateTime.substring(22, len);
    getDeliveryRecord(param, '#tableTpl', '#table');
    return false;
  });

  //执行一个laydate实例
  laydate.render({
    elem: '#datetime' //指定元素
    , type: 'datetime'
    , range: '到'
    , format: 'yyyy-MM-dd HH:mm:ss'
  });

  // 分页
  laypage.render({
    elem: 'pager',
    count: 500 //数据总数，从服务端得到
  });

  // 查询通知
  getDeliveryRecord(param, '#tableTpl', '#table');
  function getDeliveryRecord(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/biz/deliver/list',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getDeliveryRecord(param, '#tableTpl', '#table');
              }
            }
          });
        }
      }
    });
  }
});