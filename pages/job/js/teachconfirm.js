/**
 * Created by zzy on 2019/1/16.
 */
layui.use(['form'], function() {
  var form = layui.form;
  var teachConfirm = JSON.parse(localStorage.getItem('teachConfirm'));

  var confirmParam = {
    childId: teachConfirm.childId,
    visitorName: '',
    visitorCardnum: '',
    visitorPhone: '',
    visitorPhoto: '',
    visitorDescribe: '',
    isparent: teachConfirm.isparent,
    applyId: teachConfirm.applyId,
    type: teachConfirm.type,
    passId: teachConfirm.passId
  };

  //自定义验证规则
  form.verify({
    phone: [/^[1]([3-9])[0-9]{9}$/, '请检查手机号码格式是否正确']
    , applyName: [/^[\u4e00-\u9fa5]{2,10}$/, '姓名必须是2-10位的中文']
    , cardNum: [/^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/, '请检查身份证号码格式是否正确']
  });

  // 上传照片
  var MAXFILESIZE = 5242880; // 5M
  window.fileSelected = function () {
    var imgBase64 = '';
    var oFile = document.getElementById('imageFile').files[0];
    var rFilter = /^(image\/bmp|image\/gif|image\/jpeg|image\/png|image\/tiff)$/i;
    if (!rFilter.test(oFile.type)) {
      layer.msg("文件格式必须为图片");
      return;
    }
    if (oFile.size > MAXFILESIZE) {
      layer.msg("图片大小不能超过2M");
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(oFile);
    reader.onload = function (e) {
      imgBase64 = e.target.result;

      $('#resultImg').attr('src', imgBase64);
      confirmParam.visitorPhoto = imgBase64.split(',')[1];

      $('.upload-content').hide();
      $('.upload-result').show();
    }
  };

  form.on('submit(confirm)', function(data) {
    var df = data.field;
    confirmParam.visitorName = df.applyName;
    confirmParam.visitorCardnum = df.cardNum;
    confirmParam.visitorPhone = df.phone;
    confirmParam.visitorDescribe = df.memo;

    if (confirmParam.visitorPhoto === '') {
      layer.msg('请上传照片', {icon: 5, anim: 6});
      return false;
    }

    teachConfirmFunc(confirmParam)
  });

  function teachConfirmFunc(data) {
    new $Ajax({
      type: 'post',
      url: '/biz/deliver/addPerson',
      isShowLoader: false,
      dataType: 'json',
      param: data,
      callback: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          // 获得frame索引
          var index = parent.layer.getFrameIndex(window.name);
          //关闭当前frame
          parent.layer.close(index);
          parent.window.getStudentList();
        }
      }
    });
  }

  window.photoHandle = function (data) {
    var aCanvas = document.getElementById('canvas');
    var ctx = aCanvas.getContext('2d');
    var imgBase64 = '';
    ctx.drawImage(data, 0, 0, 300, 225); // 将获取视频绘制在画布上
    $('.upload-content').hide();
    $('.upload-result').show();
    imgBase64 = aCanvas.toDataURL();
    $('#resultImg').attr('src', imgBase64);

    confirmParam.visitorPhoto = imgBase64.split(',')[1];
  }
});

// 拍照完成后回调函数
function getPhoto(data) {
  photoHandle(data);
}