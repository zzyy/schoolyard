/**
 * Created by zzy on 2019/1/10.
 */
$(function () {
  var id = getQueryString('id');

  // 获取详情
  new $Ajax({
    type: 'post',
    url: '/biz/notice/info/' + id,
    isShowLoader: false,
    dataType: 'json',
    callback: function (res) {
      if (res.code === 200) {
        var arr = ['createName', 'title', 'content', 'className', 'personType', 'createTime'];
        render(arr, res.content);
      }
    }
  });
});

function render(arr, data) {
  for (var i = 0; i < arr.length; i++) {
    $('#' + arr[i]).html(data[arr[i]]);
  }
}