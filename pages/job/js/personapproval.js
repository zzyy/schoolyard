/**
 * Created by zzy on 2019/1/12.
 */
layui.config({
  base: '../../lib/dtree/'
}).extend({
  treeSelect: 'dtree'
});
layui.use(['laydate', 'element', 'form', 'dtree'], function () {
  var laydate = layui.laydate,
      laypage = layui.laypage,
      element = layui.element,
      dtree = layui.dtree,
      form = layui.form;
  var personExamine = {
    page: 1,
    limit: 10,
    isExamine: 2, // 1：已审核  2：待审核
    personName: '',
    childName: '',
    classCode: '',
    beginTime: '',
    endTime: '',
    cardNum: ''
  };

  getTableData(personExamine, '#dspTableTpl', '#dspTable', 'pager1');

  //执行一个laydate实例
  laydate.render({
    elem: '#datetime1' //指定元素
    ,type: 'datetime'
    ,range: '到'
    ,format: 'yyyy-MM-dd HH:mm:ss'
  });
  laydate.render({
    elem: '#datetime2' //指定元素
    ,type: 'datetime'
    ,range: '到'
    ,format: 'yyyy-MM-dd HH:mm:ss'
  });

  // 待审批条件查询
  form.on('submit(sreach1)', function (data) {
    personExamine.page = 1;
    personExamine.isExamine = 2;
    personExamine.personName = data.field.parent;
    personExamine.cardNum = data.field.cardNum;
    personExamine.classCode = $('#classes1').attr('data-id');
    var dateTime = data.field.datetime1;
    if (dateTime) {
      var len = dateTime.length;
      personExamine.beginTime = dateTime.substring(0, 19);
      personExamine.endTime = dateTime.substring(22, len);
    } else {
      personExamine.beginTime = '';
      personExamine.endTime = '';
    }
    getTableData(personExamine, '#dspTableTpl', '#dspTable', 'pager1');
    return false;
  });

  // 已审批条件查询
  form.on('submit(sreach2)', function (data) {
    personExamine.page = 1;
    personExamine.isExamine = 1;
    personExamine.personName = data.field.parent;
    personExamine.cardNum = data.field.cardNum;
    personExamine.classCode = $('#classes2').attr('data-id');
    var dateTime = data.field.datetime2;
    if (dateTime) {
      var len = dateTime.length;
      personExamine.beginTime = dateTime.substring(0, 19);
      personExamine.endTime = dateTime.substring(22, len);
    } else {
      personExamine.beginTime = '';
      personExamine.endTime = '';
    }
    getTableData(personExamine, '#yspTableTpl', '#yspTable', 'pager2');
    return false;
  });

  //一些事件监听
  element.on('tab(myTab)', function(data){
    if (data.index === 0) {
      personExamine.isExamine = 2;
      getTableData(personExamine, '#dspTableTpl', '#dspTable', 'pager1');
    } else if (data.index === 1) {
      personExamine.isExamine = 1;
      getTableData(personExamine, '#yspTableTpl', '#yspTable', 'pager2');
    }
  });

  // 通过操作
  $('body').on('click', '.pass', function() {
    var id = $(this).attr('data-id');
    layer.confirm('您确定通过该条审批吗？', {
      title: '提示',
      btn: ['确定','取消']
    }, function(index){
      examine(id, 1);
      layer.close(index);
    });
  });
  // 驳回操作
  $('body').on('click', '.no-pass', function() {
    var id = $(this).attr('data-id');
    layer.confirm('您确定驳回该条审批吗？', {
      title: '提示',
      btn: ['确定','取消']
    }, function(index){
      examine(id, 0);
      layer.close(index);
    });
  });

  // 查询审批列表
  function getTableData(data, tpl, el, pager) {
    new $Ajax({
      type: 'post',
      url: '/biz/person/personExamineList',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('.count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: pager,
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function(obj, first){
              if (!first) {
                data.page = obj.curr;
                getTableData(data, tpl, el, pager);
              }
            }
          });
        }
      }
    });
  }

  // 人员审批操作方法
  function examine(id, type) {
    var param = {
      ids: [],
      isExamine: type // 是否通过审核|0:否,1:是
    };
    param.ids.push(id);

    new $Ajax({
      type: 'post',
      url: '/biz/person/isExamine',
      isShowLoader: false,
      param: param,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          layer.msg(res.message);
          getTableData(personExamine, '#dspTableTpl', '#dspTable', 'pager1');
        }
      }
    });
  }
});

// 点击班级输入框弹出班级树选择
$('.classes').on('click', function () {
  selectClass();
});