/**
 * Created by zzy on 2019/1/15.
 */
layui.use(['element', 'layer'], function () {
  var element = layui.element,
    layer = layui.layer;
  var studentParams = {
    personName: '',
    classCode: '',
    personState: '0',
    limit: 1000
  };

  getClassList();

  // tab事件监听
  element.on('tab(classTab)', function () {
    studentParams.classCode = $(this).attr('data-code');
    getStudentList();
  });
  // 点击学生列表操作
  $('body').on('click', '.student', function (e) {
    var self = $(this);
    var id = self.attr('data-id');
    var state = self.attr('data-state');
    getPersonList(id, state, e, self);
  });
  // 点击人员操作
  $('body').on('click', '.person', function (e) {
    var self = $(this);
    var confirmParam = {
      childId: returnNull(self.attr('data-child-id')),
      id: returnNull(self.attr('data-id')),
      isparent: '',
      applyId: returnNull(self.attr('data-apply-id')),
      type: returnNull(self.attr('data-type')),
      passId: returnNull(self.attr('data-pass'))
    };
    var jump = self.attr('data-jump');

    // 关闭人员列表
    self.parents('.student-dialog').remove();
    if (jump === '0') {
      teacherConfirm(confirmParam);
    } else if (jump === '1') {
      localStorage.setItem('teachConfirm', JSON.stringify(confirmParam));
      layer.open({
        type: 2,
        area: ['710px', '620px'],
        fix: false, //不固定
        maxmin: true,
        shadeClose: true,
        shade: 0.4,
        title: '老师确认',
        content: './teachconfirm.html',
        yes: function (index, layero) {
          var iframeWin = window[layero.find('iframe')[0]['name']];
          // var $input = iframeWin.document.getElementById('idInput');

        },
        btn2: function (index, layero) {

        }
      });
    }
    e.stopPropagation();
  });
  // 关闭弹窗
  $('body').on('click', '.close', function (e) {
    $(this).parents('.student-dialog').remove();
    e.stopPropagation();
  });

  // 获取班级
  function getClassList() {
    new $Ajax({
      type: 'post',
      url: '/biz/deliver/getListForClass',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var classList = res.content;
          if (classList.length) {
            var $classTab = $('#classTab');
            var $tabContent = $('#tabContent');
            $classTab.html('');
            $tabContent.html('');
            for (var i = 0; i < classList.length; i++) {
              // 过滤未分配班级
              if (classList[i].classCode !== '10000001') {
                // tab标题
                var title = '<li data-code="' + classList[i].classCode + '">' + classList[i].className + '</li>';
                // tab内容
                var content = '<div class="layui-tab-item">' +
                  '<ul class="flow-default layui-row" id="classes-' + classList[i].classCode + '">' +
                  '</ul>' +
                  '</div>';
                $classTab.append(title);
                $tabContent.append(content);
              }
            }
            // 给第一个li添加选中样式
            $classTab.children(':first').addClass('layui-this');
            $tabContent.children(':first').addClass('layui-show');

            // 调用学生列表查询方法
            studentParams.classCode = classList[0].classCode;
            getStudentList();
          }
        }
      }
    });
  }

  // 获取学生列表
  window.getStudentList = function () {
    new $Ajax({
      type: 'post',
      url: '/biz/person/listForClasses',
      isShowLoader: false,
      dataType: 'json',
      param: studentParams,
      callback: function (res) {
        if (res.code === 200) {
          var list = res.content.list;
          var $class = $('#classes-' + studentParams.classCode);
          $class.html('');
          for (var i = 0; i < list.length; i++) {
            var li = '<li class="layui-col-xs2 student" data-id="' + list[i].id + '" data-state="' + list[i].personState + '">' +
              '<span class="wait wait-' + list[i].personState + '" style="display: none;"></span>' +
              '<div class="img-wrap">' +
                '<img src="' + public.HTTP_URL + list[i].photo + '">' +
              '</div>' +
              '<p class="name">' + list[i].personName + '</p>' +
              '</li>';
            $class.append(li);
          }
          $('.wait-2').show();
        }
      }
    });
  }

  // 获取学生所对应的接人列表
  function getPersonList(id, state, e, self) {
    if (state === '2') {
      $('.student-dialog').remove();
      layer.msg('等待家长确认');
      return false;
    };
    new $Ajax({
      type: 'post',
      url: '/biz/deliver/parentListR/' + id,
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var personList = res.content;
          // 创建人员列表框
          $('.student-dialog').remove();
          var x = e.pageX - self.offset().left;
          var y = e.pageY - self.offset().top;
          var dialog = '<div class="student-dialog"><span class="layui-icon close">&#x1007;</span><ul class="person-list"></ul></div>';
          self.append(dialog);
          $('.student-dialog').css({'left': x, 'top': y});
          // 循环人员数据并渲染
          for (var i = 0; i < personList.length; i++) {
            var person = '<li class="person person-' + i + '"' +
              ' data-id="' + personList[i].id + '"' +
              ' data-jump="' + personList[i].jump + '"' +
              ' data-type="' + personList[i].type + '"' +
              ' data-pass="' + personList[i].passId + '"' +
              ' data-child-id="' + personList[i].childId + '"' +
              ' data-apply-id="' + personList[i].applyId + '"' +
              '>' + personList[i].personName + '</li>';

            $('.person-list').append(person);

            if (personList[i].passId && personList[i].passId !== '') {
              $('.person-' + i).addClass('high-light');
            }
          }
        }
      }
    });
  }

  // 老师确认提交
  function teacherConfirm(data) {
    new $Ajax({
      type: 'post',
      url: '/biz/deliver/tracherConfirmR',
      isShowLoader: false,
      dataType: 'json',
      param: data,
      callback: function (res) {
        layer.msg(res.message);
        if (res.code === 200) {
          getStudentList();
        }
      }
    });
  }
});

// 如果值为null或undefined则返回空
function returnNull(val) {
  if (val === 'undefined' || val === null) {
    return '';
  } else {
    return val;
  }
}