layui.config({
  base: '../../lib/dtree/'
}).extend({
  treeSelect: 'dtree'
});

layui.use(['laydate', 'layer', 'element', 'form'], function () {
  var laydate = layui.laydate,
    layer = layui.layer,
    form = layui.form;
  var param = {
    id: '',
    faceTerminalName: '',
    faceTerminalAddress: '',
    faceTerminalChannel: '',
    faceTerminalStatus: '',
    regTime: '',
    faceTerminalIp: '',
    faceTerminalPort: '',
    faceTerminalMac: '',
    faceTerminalDriverId: '',
    reportMode: '',
    streamMode: '',
    authorizationNum: '',
    authorizationExamineTime: '',
    areaId: '',
    faceTerminalType: ''
  };


  var id = getQueryString('id');
  // 设备详情
  var url = '/sys/faceterminal/info/' + id;
  new $Ajax({
    type: 'post',
    url: url,
    isShowLoader: false,
    dataType: 'json',
    callback: function (res) {
      if (res.code === 200) {
        $('#deviceName').val(res.content.faceTerminalName);
        $('#faceTerminalAddress').val(res.content.faceTerminalAddress);
        $('#faceTerminalChannel').val(res.content.faceTerminalChannel);
        $('#faceTerminalStatus').val(res.content.faceTerminalStatus);
        $('#regTime').val(res.content.regTime);
        $('#ipAddr').val(res.content.faceTerminalIp);
        $('#port').val(res.content.faceTerminalPort);
        $('#faceTerminalMac').val(res.content.faceTerminalMac);
        $('#faceTerminalDriverId').val(res.content.faceTerminalDriverId);
        $('#mode').val(res.content.reportMode);
        $('#streamMode').val(res.content.streamMode);
        $('#authorizationNum').val(res.content.authorizationNum);
        $('#authorizationExamineTime').val(res.content.authorizationExamineTime);
        $('#device').val(res.content.faceTerminalType);
        $('#area').val(res.content.areaDesc);
        form.render();
      }
    }
  });

  //自定义验证规则
  form.verify({
    deviceName: [/^[^ \r\n\s]{1,6}$/, '标题不能包含空格且不能大于6位']
    ,
    faceTerminalAddress: [/^[^ \r\n\s]{1,10}$/, '位置不能包含空格且不能大于10位']
    ,
    ipAddr: [/^(?:(?:2[0-4][0-9]\.)|(?:25[0-5]\.)|(?:1[0-9][0-9]\.)|(?:[1-9][0-9]\.)|(?:[0-9]\.)){3}(?:(?:2[0-5][0-5])|(?:25[0-5])|(?:1[0-9][0-9])|(?:[1-9][0-9])|(?:[0-9]))$/, '请检查IP地址是否正确']
    ,
    macAddr: [/([A-Fa-f0-9]{2}-){5}[A-Fa-f0-9]{2}/, '请检查MAC地址是否正确']
  });

  laydate.render({
    elem: '#authorizationExamineTime' //指定元素
    , type: 'datetime'
    , format: 'yyyy-MM-dd HH:mm:ss'
  });

  laydate.render({
    elem: '#regTime' //指定元素
    , type: 'datetime'
    , format: 'yyyy-MM-dd HH:mm:ss'
  });

  //监听提交
  form.on('submit(add)', function (data) {
    if (/[\u4E00-\u9FA5]/g.test(data.field.faceTerminalDriverId)) {
      layer.msg('驱动ID不能包含汉字', {icon: 5, anim: 6});
      return false;
    }
    if (/[\u4E00-\u9FA5]/g.test(data.field.authorizationNum)) {
      layer.msg('授权编号不能包含汉字', {icon: 5, anim: 6});
      return false;
    }

    if (data.field.faceTerminalMac.length > 17) {
      layer.msg('请检查MAC地址是否正确', {icon: 5, anim: 6});
      return false;
    }
    if (data.field.authorizationNum.length > 22) {
      layer.msg('请检查授权编码是否正确', {icon: 5, anim: 6});
      return false;
    }
    if (data.field.port.length > 5) {
      layer.msg('端口字段过长', {icon: 5, anim: 6});
      return false;
    }

    param.id = id;
    param.faceTerminalName = data.field.deviceName;
    param.faceTerminalAddress = data.field.faceTerminalAddress;
    param.faceTerminalChannel = data.field.faceTerminalChannel;
    param.faceTerminalStatus = data.field.faceTerminalStatus;
    param.regTime = data.field.regTime;
    param.faceTerminalIp = data.field.ipAddr;
    param.faceTerminalPort = data.field.port;
    param.faceTerminalMac = data.field.faceTerminalMac;
    param.faceTerminalDriverId = data.field.faceTerminalDriverId;
    param.reportMode = data.field.mode;
    param.streamMode = data.field.streamMode;
    param.authorizationNum = data.field.authorizationNum;
    param.authorizationExamineTime = data.field.authorizationExamineTime;
    param.faceTerminalType = data.field.type;
    param.areaId = $('#area').attr('data-id');
    console.log(param);
    addDevice(param);
    return false;
  });

  // 修改设备信息
  function addDevice(data) {
    new $Ajax({
      type: 'post',
      url: '/sys/faceterminal/update',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        layer.msg(res.message);
        var index = parent.layer.getFrameIndex(window.name);
        if (res.code === 200) {
          layer.msg(res.message);
          parent.layer.close(index);
          parent.location.reload();
        } else if (res.code === 500) {
          return false;
        }
      }
    });
  }
});

renderDevice();

function renderDevice() {
  var deviceDic = JSON.parse(localStorage.getItem('deviceDic'));
  var placeholder = '<option value="">请选择设备类型</option>';
  $('#device').html('').append(placeholder);
  for (var i = 0; i < deviceDic.length; i++) {
    var opt = '<option value="' + deviceDic[i].dicClassCode + '">' + deviceDic[i].classNameCn + '</option>';
    $('#device').append(opt);
  }
}

// 点击区域输入框弹出区域树选择
$('.areas').on('click', function () {
  var self = $(this);
  layer.open({
    type: 2,
    area: ['420px', '420px'],
    fix: false, //不固定
    maxmin: true,
    shadeClose: true,
    shade: 0.4,
    title: '区域',
    content: '../public/areaTree.html',
    btn: ['确定', '重置'],
    btnAlign: 'c',
    yes: function (index, layero) {
      var iframeWin = window[layero.find('iframe')[0]['name']];
      var $input = iframeWin.document.getElementById('idInput');
      var content = $input.value;
      var id = $input.getAttribute('data-id');
      self.val(content).attr('data-id', id);
      layer.close(index); //如果设定了yes回调，需进行手工关闭
    },
    btn2: function (index, layero) {
      var iframeWin = window[layero.find('iframe')[0]['name']];
      var $input = iframeWin.document.getElementById('idInput');
      $input.value = '';
      $input.setAttribute('data-id', '');
      self.val('').attr('data-id', '');
    }
  });
});