/**
 * Created by zzy on 2019/1/17.
 */
layui.config({
  base: '../../lib/dtree/'
}).extend({
  treeSelect: 'dtree'
});
layui.use(['dtree', 'form'], function () {
  var dtree = layui.dtree,
      form = layui.form;
  var dtreeEle,
      treeParam;

  //自定义验证规则
  form.verify({
    childName: [/^[a-zA-Z\d\u4e00-\u9fa5]{0,15}$/, '节点名称长度不得超过15个字符且不得包含特殊字符']
  });

  // 新增
  var addParam = {
    parentId: '',
    name: '',
    level: '',
    type: ''
  };
  var updateParam = {
    id: '',
    parentId: '',
    name: '',
    level: '',
    type: ''
  };

  getClassesTree();

  $('#add').on('click', function () {
    treeParam = dtree.getNowParam(dtreeEle);
    $('.form-wrap').hide();
    $('#addForm').show();
  });
  $('#modify').on('click', function () {
    treeParam = dtree.getNowParam(dtreeEle);
    $('.form-wrap').hide();
    $('#modifyForm').show();
  });
  $('#delete').on('click', function () {
    treeParam = dtree.getNowParam(dtreeEle);
    $('.form-wrap').hide();
    $('#deleteForm').show();
  });

  // 新增
  form.on('submit(add)', function(data) {
    if (!data.field.parentName) {
      layer.msg('请选择上级节点！', {icon: 5, anim: 6});
      return false;
    }
    var isLast = JSON.parse(treeParam.recordData).type;
    if (isLast === '0') {
      layer.msg('该节点是最下级教室，不能再新增子节点！', {icon: 5, anim: 6});
      return false;
    }
    addParam.parentId = treeParam.nodeId;
    addParam.name = data.field.childName;
    addParam.level = Number(treeParam.level) + 1;
    if (data.field.type) {
      addParam.type = data.field.type;
    } else {
      addParam.type = '1';
    }
    controlReq('/sys/area/add', addParam);
  });
  // 修改
  form.on('submit(update)', function(data) {
    updateParam.id = treeParam.nodeId;
    updateParam.parentId = treeParam.parentId;
    updateParam.name = data.field.childName;
    updateParam.level = treeParam.level;
    if (data.field.type) {
      updateParam.type = data.field.type;
    } else {
      updateParam.type = '1';
    }
    controlReq('/sys/area/update', updateParam);
  });
  // 删除
  $('#delBtn').on('click', function () {
    var id = $(this).attr('data-id');
    if (id) {
      deleteReq(id);
    } else {
      layer.msg('请选择您要删除的节点！', {icon: 5, anim: 6});
    }
  });
  // 取消删除
  $('#cancelDel').on('click', function () {
    var cxt = '请选择左侧您要删除的节点，点击确定删除。';
    $('#delDialog').html(cxt);
    $('#delBtn').attr('data-id', '');
    getClassesTree();
  });

  // 点击树节点获取参数
  dtree.on("node('classesTree')", function(obj){
    $('.parent').val(obj.param.context).attr('data-id', obj.param.nodeId);
    treeParam = obj.param;
    // 更新用的
    var isLast = JSON.parse(treeParam.recordData).type;
    if (isLast === '0') {
      $('#updateType').prop('checked', true);
    } else {
      $('#updateType').prop('checked', false);
    }
    form.render();
    // 删除用的
    var cxt = '<div>您确定要删除 [ <span id="nodeName">' + obj.param.context + '</span> ] 吗？</div>';
    $('#delDialog').html(cxt);
    $('#delBtn').attr('data-id', obj.param.nodeId);
    $('.btn-wrap').show();
  });

  // 获取区域树
  function getClassesTree() {
    new $Ajax({
      type: 'post',
      url: '/sys/area/select',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.content;

          // 渲染树，使用data渲染
          dtreeEle = dtree.render({
            elem: '#classesTree',
            data: data,
            checkbar: true,
            dot: false,
            initLevel: 100,
            skin: 'layui',
            record: true,
            response: {
              title: 'name',
              childName: 'list',
              checkArr: '1'
            }
          });
        }
      }
    });
  }

  // 新增和修改
  function controlReq(url, data) {
    new $Ajax({
      type: 'post',
      url: url,
      isShowLoader: false,
      dataType: 'json',
      param: data,
      callback: function (res) {
        if (res.code === 200) {
          layer.msg(res.message, {icon: 6});
          getClassesTree();
          $('input').val('');
          $('.type').prop('checked', false);
          form.render();
        } else {
          layer.msg(res.message, {icon: 5, anim: 6});
        }
      }
    });
  }
  // 删除节点
  function deleteReq(id) {
    new $Ajax({
      type: 'post',
      url: '/sys/area/delete/' + id,
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          layer.msg(res.message, {icon: 6});
          getClassesTree();
          // 删除页面
          var cxt = '<div>删除成功！</div>';
          $('#delDialog').html(cxt);
          $('#delBtn').attr('data-id', '');
          $('.btn-wrap').hide();
        } else {
          layer.msg(res.message, {icon: 5, anim: 6});
        }
      }
    });
  }
});