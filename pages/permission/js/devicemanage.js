layui.config({
  base: '../../lib/dtree/'
}).extend({
  treeSelect: 'dtree'
});
layui.use(['laydate', 'element', 'form', 'dtree'], function () {
  var laydate = layui.laydate,
    laypage = layui.laypage,
    element = layui.element,
    dtree = layui.dtree,
    $ = layui.jquery,
    form = layui.form;
  var param = {
    page: 1,
    limit: 10,
    faceTerminalMac: '',
    faceTerminalIp: '',
    faceTerminalType: '',
    reportMode: '',
    areas: [],
    sidx: 'id',
    order: 'desc'
  };

  renderDevice();
  getTableData(param, '#deviceTableTpl', '#deviceTable');

  //监听提交
  form.on('submit(sreach)', function (data) {
    param.page = 1;
    param.areas = [];
    param.faceTerminalMac = data.field.mac;
    param.faceTerminalIp = data.field.ipaddr;
    param.faceTerminalType = data.field.type;
    param.reportMode = data.field.mode;
    param.areas.push($('#area').attr('data-id'));
    param.areas = skipEmptyElementForArray(param.areas);//如果areas存在空字符串''，则去掉该空字符串
    var dateTime = data.field.datetime;
    if (dateTime) {
      param.regTimeStart = dateTime.substring(0, 19);
      var len = dateTime.length;
      param.regTimeEnd = dateTime.substring(22, len);
    } else {
      param.regTimeStart = '';
      param.regTimeEnd = '';
    }

    getTableData(param, '#deviceTableTpl', '#deviceTable');
    return false;
  });

  // 执行一个laydate实例
  laydate.render({
    elem: '#datetime' //指定元素
    , type: 'datetime'
    , range: '到'
    , format: 'yyyy-MM-dd HH:mm:ss'
  });

  // 查询设备列表
  function getTableData(data, tpl, el) {
    new $Ajax({
      type: 'post',
      url: '/sys/faceterminal/list',
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;

          tableData.forEach(function (item) {
            var type = transType(item.faceTerminalType);
            item.faceTerminalTypeCn = type;

            if (item.reportMode === '0') {
              item.reportModeCn = '主动上报'
            } else if (item.reportMode === '1') {
              item.reportModeCn = '被动上报'
            }

            if (item.faceTerminalStatus === '0') {
              item.faceTerminalStatusCn = '异常'
            } else if (item.faceTerminalStatus === '1') {
              item.faceTerminalStatusCn = '在线'
            } else if (item.faceTerminalStatus === '2') {
              item.faceTerminalStatusCn = '不在线'
            }
          });
          
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);
          $('#count').html(res.content.totalCount);

          // 分页
          laypage.render({
            elem: 'pager',
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function (obj, first) {
              if (!first) {
                data.page = obj.curr;
                getTableData(param, '#deviceTableTpl', '#deviceTable');
              }
            }
          });
        }
      }
    });
  }

  $('body').on('click', '.delete', function () {
    var id = $(this).attr('data-id');
    member_del(id);
  });
  /*删除*/
  function member_del(id) {
    var delParam = [];
    delParam.push(id);
    layer.confirm('确认要删除吗？', function () {
      //发异步删除数据
      new $Ajax({
        type: 'post',
        url: '/sys/faceterminal/delete',
        isShowLoader: false,
        param: delParam,
        dataType: 'json',
        callback: function (res) {
          if (res.code === 200) {
            getTableData(param, '#deviceTableTpl', '#deviceTable');
            layer.msg(res.message, {icon: 1, time: 1000});
          }
        }
      });
    });
  }
  // 渲染设备下拉
  function renderDevice () {
    var deviceDic = JSON.parse(localStorage.getItem('deviceDic'));
    var placeholder = '<option value="">请选择设备类型</option>';
    $('#device').append(placeholder);
    for (var i = 0; i < deviceDic.length; i++) {
      var opt = '<option value="' + deviceDic[i].dicClassCode + '">' + deviceDic[i].classNameCn + '</option>';
      $('#device').append(opt);
    }

    form.render();
  }

  // 点击区域输入框弹出区域树选择
  $('.areas').on('click', function () {
    var self = $(this);
    layer.open({
      type: 2,
      area: ['710px', '420px'],
      fix: false, //不固定
      maxmin: true,
      shadeClose: true,
      shade: 0.4,
      title: '区域树',
      content: '../public/areaTree.html',
      btn: ['确定', '重置'],
      btnAlign: 'c',
      yes: function (index, layero) {
        var iframeWin = window[layero.find('iframe')[0]['name']];
        var $input = iframeWin.document.getElementById('idInput');
        var content = $input.value;
        var id = $input.getAttribute('data-id');
        self.val(content).attr('data-id', id);
        layer.close(index); //如果设定了yes回调，需进行手工关闭
      },
      btn2: function (index, layero) {
        var iframeWin = window[layero.find('iframe')[0]['name']];
        var $input = iframeWin.document.getElementById('idInput');
        $input.value = '';
        $input.setAttribute('data-id', '');
        self.val('').attr('data-id', '');
      }
    });
  });

  function skipEmptyElementForArray(arr) {
    var a = [];
    $.each(arr, function (i, v) {
      var data = $.trim(v);
      if ('' != data) {
        a.push(data);
      }
    });
    return a;
  }

  // 根据设备类型key获取对应的值
  function transType(key) {
    var typeList = JSON.parse(localStorage.getItem('deviceDic'));
    for (var i = 0; i < typeList.length; i++) {
      if (key === typeList[i].dicClassCode) {
        return typeList[i].classNameCn;
      }
    }
  }
});