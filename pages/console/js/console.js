/**
 * Created by zzy on 2018/12/25.
 */
// 激活layui控件，本页面是折叠面板
layui.use(['element', 'layer'], function(){
  var $ = layui.jquery;
  var element = layui.element;
  var laypage = layui.laypage;
  
  getChartData('noIn');
  showDate();
  applyUserInfo();
  applyCount();

  // 切换tab更改chart显示数据
  element.on('tab(dataCount)', function(data){
    var idx = data.index + 1;
    switch (idx) {
      case 1:
        getChartData('noIn');
        break;
      case 2:
        getChartData('exceptionDeliver');
        break;
      case 3:
        getChartData('exceptionMeet');
        break;
    }
  });

  // 传参
  var shuttleapplyParam = {
    page: 1,
    limit: 20,
    type: 1 // 0-接，1-送
  };
  var personExamine = {
    page: 1,
    limit: 20,
    isExamine: 2 // 1：已审核  2：待审核
  };
  var confirm = {
    page: 1,
    limit: 20
  };
  getTableData('/shuttleapply/list', shuttleapplyParam, '#djTableTpl', '#dsTable', 0);
  // 中间部分点击切换
  $('.task').on('click', function(){
    var self = $(this);
    var index = self.attr('data-index');
    $('.task').removeClass('select-this');
    self.addClass('select-this');

    // 先隐藏所有表格
    $('.table-wrap .table-item').hide();
    $('#table-' + index).show();

    switch (index) {
      case '0':
        // 代送
        shuttleapplyParam.type = 1;
        getTableData('/shuttleapply/list', shuttleapplyParam, '#djTableTpl', '#dsTable', index);
        break;
      case '1':
        // 代接
        shuttleapplyParam.type = 0;
        getTableData('/shuttleapply/list', shuttleapplyParam, '#djTableTpl', '#djTable', index);
        break;
      case '2':
        // 人员审批
        getTableData('/person/personExamineList', personExamine, '#spTableTpl', '#spTable', index);
        break;
      case '3':
        // 我发起的-家长二次确认
        getTableData('/notice/confirmList', confirm, '#fqTableTpl', '#fqTable', index);
        break;
    }
  });

  // 右上角日期
  setInterval(function () {
    showDate();
  }, 1000);

  // 获取个人信息
  function applyUserInfo() {
    new $Ajax({
      type: 'post',
      url: '/biz/workPlatform/personMessage',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          // 基本信息
          $('#username').html(res.content.personName || '无');
          $('#sex').html(res.content.sex || '无');
          $('#role').html(res.content.roleName || '无');
          $('#avatar').attr('src', public.HTTP_URL + res.content.photo);
          // 工作图表
          var approve = [],
              notice = [];
          approve.push(res.content.today.approve);
          approve.push(res.content.month.approve);
          notice.push(res.content.today.notice);
          notice.push(res.content.month.notice);
          initJobChart(approve, notice);
        }
      }
    });
  }

  // 获取班级概况
  new $Ajax({
    type: 'post',
    url: '/biz/workPlatform/countClass',
    isShowLoader: false,
    dataType: 'json',
    callback: function (res) {
      if (res.code === 200) {
        var classData = res.content;
        var classTpl = $('#classTpl').html();
        var classHtml = juicer(classTpl, {"classData": classData});
        $('#classIntro').html(classHtml);
      }
    }
  });

  // 获取中间统计面板数据
  function applyCount() {
    new $Ajax({
      type: 'post',
      url: '/biz/shuttleapply/getShuttleApplyCount',
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          $('#dsCount').html(res.content.dsCount);
          $('#djCount').html(res.content.djCount);
          $('#examineCount').html(res.content.examineCount);
          $('#noticeCount').html(res.content.noticeCount);
        }
      }
    });
  }

  // 右下角图表
  function getChartData(url) {
    new $Ajax({
      type: 'post',
      url: '/biz/workPlatform/' + url,
      isShowLoader: false,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var data = res.content;
          countChart('chartContent', data.classes, data.school, data.xData);
        }
      }
    });
  }

// 中间表格
  function getTableData(url, data, tpl, el, index) {
    new $Ajax({
      type: 'post',
      url: '/biz' + url,
      isShowLoader: false,
      param: data,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          var tableData = res.content.list;
          var tableTpl = $(tpl).html();
          var tableHtml = juicer(tableTpl, {'tableList': tableData});
          $(el).html(tableHtml);

          // 分页
          laypage.render({
            elem: 'pager' + index,
            curr: res.content.currPage,
            limit: res.content.pageSize,
            count: res.content.totalCount,
            jump: function(obj, first){
              if (!first) {
                data.page = obj.curr;
                getTableData(url, data, tpl, el, index);
              }
            }
          });
        }
      }
    });
  }

  // 通过操作
  $('body').on('click', '.pass', function() {
    var id = $(this).attr('data-id');
    layer.confirm('您确定通过该条审批吗？', {
      title: '提示',
      btn: ['确定','取消']
    }, function(index){
      examine(id, 1);
      layer.close(index);
    });
  });
  // 驳回操作
  $('body').on('click', '.no-pass', function() {
    var id = $(this).attr('data-id');
    layer.confirm('您确定驳回该条审批吗？', {
      title: '提示',
      btn: ['确定','取消']
    }, function(index){
      examine(id, 0);
      layer.close(index);
    });
  });
  // 人员审批操作方法
  function examine(id, type) {
    var param = {
      ids: [],
      isExamine: type // 是否通过审核|0:否,1:是
    };
    param.ids.push(id);

    new $Ajax({
      type: 'post',
      url: '/biz/person/isExamine',
      isShowLoader: false,
      param: param,
      dataType: 'json',
      callback: function (res) {
        if (res.code === 200) {
          layer.msg(res.message);
          getTableData('/person/personExamineList', personExamine, '#spTableTpl', '#spTable', 2);
          applyCount();
          applyUserInfo();
        }
      }
    });
  }

// 折叠面板收起展开操作
  $('body').on('click', '.my-colla', function () {
    var $elem = $(this).parent('.layui-colla-item').find('.layui-colla-content');
    var isShow = $elem.hasClass('layui-show');
    if (isShow) {
      $(this).find('.layui-colla-icon').html('&#xe602;');
      $elem.removeClass('layui-show');
    } else {
      $(this).find('.layui-colla-icon').html('&#xe61a;');
      $elem.addClass('layui-show');
    }
  });

// 时间显示方法
  function showDate() {
    var d = new Date();
    var date = d.Format('yyyy年MM月dd日');
    var time = d.Format('HH:mm:ss');
    var week = getWeek(d.getDay());
    var hour = getHours(d.getHours());
    $('#date').html(date + ' ' + week + hour);
    $('#time').html(time);
  }

// 左侧老师个人工作图表
  function initJobChart(approve, notice) {
    // 基于准备好的dom，初始化echarts实例
    var jobChart = echarts.init(document.getElementById('jobChart'));
// 指定图表的配置项和数据
    var jobOption = {
      color: ['#009688', '#52616c'],
      tooltip: {
        trigger: 'axis',
        axisPointer: { type: 'shadow' }
      },
      legend: { data: ['审批', '通知'] },
      grid: {
        left: '3%',
        right: '8%',
        bottom: '5%',
        top: '50px',
        containLabel: true
      },
      xAxis : [ { type : 'category', data: ['今日','当月'] } ],
      yAxis : { name: '次', type: 'value', minInterval: 1, boundaryGap: [0, 0.01] },
      series: [
        {
          name: '审批',
          type: 'bar',
          barWidth: '20px',
          label: { show: true, position: 'top' },
          data: approve
        },
        {
          name: '通知',
          type: 'bar',
          barWidth: '20px',
          label: { show: true, position: 'top' },
          data: notice
        }
      ]
    };
// 使用刚指定的配置项和数据显示图表。
    jobChart.setOption(jobOption);
  }
});