/**
 * Created by zzy on 2018/12/26.
 */
/*
 * 右侧tab切换部分图表，包括无进出记录、异常带接、异常带送
 * */
function countChart(el, barData, lineData, xData) {
  var chartContent = echarts.init(document.getElementById(el));
  var classes = barData;
  var school = lineData;
  var maxclasses = calMax(classes);//注册Y轴值
  var maxschool = calMax(school);//活跃Y轴值
  var interval_left = maxclasses / 5;//左轴间隔
  var interval_right = maxschool / 5;//右轴间隔

  var countOption = {
    color: ['#009688', '#52616c'],
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      data: ['班级平均值', '学校平均值']
    },
    grid: {
      left: '5%',
      right: '8%',
      bottom: '5%',
      // top: '0px',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category',
        data: xData,
        axisPointer: {
          type: 'shadow'
        },
        axisLabel: {
          interval: 0,//横轴信息全部显示
          rotate: -50,//-30度角倾斜显示
        }
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: '班级',
        min: 0,
        max: maxclasses,
        interval: interval_left,
		minInterval: 1
      },
      {
        type: 'value',
        name: '学校',
        min: 0,
        max: maxschool,
        interval: interval_right,
		minInterval: 1
      }
    ],
    series: [
      {
        name: '班级平均值',
        type: 'bar',
        barWidth: '20px',
        data: classes
      },
      {
        name: '学校平均值',
        type: 'line',
        yAxisIndex: 1,
        data: school
      }
    ]
  };

  chartContent.setOption(countOption);
}

/*
* 求最大值
* */
function calMax(arr) {
  var max = arr[0];
  for (var i = 1; i < arr.length; i++) {// 求出一组数组中的最大值
    if (max < arr[i]) {
      max = arr[i];
    }
  }
  var maxint = Math.ceil(max / 10);// 向上取整
  var maxval = maxint * 10;// 最终设置的最大值
  return maxval;// 输出最大值
}